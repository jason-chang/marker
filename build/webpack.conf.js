const path = require('path');
const { writeToDebugWebpackConfig } = require('./utils');


function chain(webpackConfig) {
  webpackConfig.resolve.alias
    .set('@app', path.resolve(__dirname, '../app'));

  webpackConfig.module
    .rule('raw')
      .test(/\.(md)$/)
        .use('raw')
          .loader('raw-loader')
          .end()
        .end();
}

function config(webpackConfig) {

}

module.exports = { chain };
