
const fs = require('fs');
const path = require('path');

function writeToDebugWebpackConfig(webpackConfig) {
  fs.writeFileSync(path.resolve(__dirname, '../webpackConfig.debug.json'), webpackConfig.toString());
}

module.exports = {
  writeToDebugWebpackConfig,
};
