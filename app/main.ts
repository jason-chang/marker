import Vue from 'vue';
import App from '@app/App.vue';
import marker from '@/index';

import './app.scss';

import '@/styles/themes/light.scss';
import '@/styles/themes/dark.scss';

Vue.use(marker);

// eslint-disable-next-line no-new
new Vue({
  render: h => h(App),
}).$mount('#app');
