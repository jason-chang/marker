
const camelCase = require('lodash/camelCase');
const upperFirst = require('lodash/upperFirst');

const packageOptions = require('./package.json');

process.env.VUE_APP_COMPONENT_ID = upperFirst(camelCase(packageOptions.componentId));
process.env.VUE_APP_VERSION = packageOptions.version;

// vue.config.js
module.exports = {
  publicPath: '/',
  pages: {
    app: {
      entry: 'app/main.ts',
      filename: 'index.html',
      template: 'public/index.html',
      title: 'Marker Demo',
      chunks: ['chunk-vendors', 'chunk-common', 'marker', 'app'],
    },
    marker: 'src/index.ts',
  },
};
