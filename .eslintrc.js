module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
  ],
  rules: {
    // 单行文本长度
    'max-len': 0,

    'class-methods-use-this': 0,

    'import/no-cycle': 0,

    'import/prefer-default-export': 0,
    // 允许倒入模块时不写扩展名
    'import/extensions': 0,
    // impont/require 后可以不留空行
    'import/newline-after-import': 0,

    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
    // 允许连续赋值
    'no-multi-assign': 0,

    'no-param-reassign': 0,

    'no-shadow': 0,
    // 允许使用 ++ --
    'no-plusplus': 0,
    // 允许空构造函数
    'no-useless-constructor': 0,
    //
    'object-curly-newline': 0,
    // 允许在代码块 首位留空行
    'padded-blocks': 0,
    // 允许不使用解构赋值
    'prefer-destructuring': 0,
    // 按需添加箭头函数参数的括号
    'arrow-parens': ['error', 'as-needed'],
    //
    'no-trailing-spaces': 0,
    // 按照环境允许 debugger 关键字
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
