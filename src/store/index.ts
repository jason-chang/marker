import clone from 'lodash/clone';
import createLogger from 'vuex/dist/logger';
import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { Layout } from '@/store/modules/Layout';
import { FindReplace } from '@/store/modules/FindReplace';
import { Editor } from '@/libs/editor/editor';

Vue.use(Vuex);

const isDebug = process.env.NODE_ENV !== 'production';

export interface RootState {
  editor?: Editor,
  light: boolean,
  canUndo: boolean,
  canRedo: boolean,

  Layout?: Layout,
  FindReplace?: FindReplace,
}

const options: StoreOptions<RootState> = {
  state: {
    editor: undefined,
    light: false,
    canUndo: false,
    canRedo: false,
  },
  modules: {
    Layout,
    FindReplace,
  },
  mutations: {
    setState(state, payload) {
      Object.assign(state, payload);
    },
  },
  getters: {},
  actions: {},
  // strict: isDebug,
  plugins: isDebug ? [createLogger()] : [],
};

export function createStore() {
  return new Vuex.Store<RootState>(clone(options));
}
