import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';

@Module({
  namespaced: true,
})
export class FindReplace extends VuexModule {

  type: string = 'find';
  lastOpen: number = 0;
  findText: string = '';
  replaceText: string = '';
  caseSensitive = false;
  useRegexp = false;

  @Mutation
  setState(payload: any) {
    Object.assign(this, payload);
  }

  @Mutation
  setLastOpen() {
    this.lastOpen = Date.now();
  }

  @Action
  open({ type, findText }: { type: string, findText: string }) {
    if (type) {
      this.context.commit('Layout/setState', { showFindReplace: true }, { root: true });
    }

    this.context.commit('setState', { type });

    if (findText) {
      this.context.commit('setState', { findText });
    }

    this.context.commit('setLastOpen');
  }

  @Action
  close() {
    this.context.commit('Layout/setState', { showFindReplace: false }, { root: true });
    this.context.commit('setState', { type: 'find' });
    this.context.commit('setState', { findText: '' });
  }
}
