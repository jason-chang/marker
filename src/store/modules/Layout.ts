import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import Vue from 'vue';

export function createToggler<T extends Vue>(name: string) {
  return function toggler(this: T) {
    this.$store.commit('Layout/toggleState', name);
  };
}

export type SwithNames = 'onlyEditor' | 'onlyPreview' | 'showStatusBar' | 'showToolbar' | 'showToc' | 'showFindReplace' | 'scrollSync';

@Module({
  namespaced: true,
})
export class Layout extends VuexModule {
  onlyEditor = false;
  onlyPreview = false;

  showStatusBar = true;
  showToolbar = true;
  showToc = false;
  showFindReplace = false;

  scrollSync = true;
  styles = {};

  get showEditor() {
    return !this.onlyPreview;
  }

  get showPreview() {
    return !this.onlyEditor;
  }

  get disableToolbar() {
    return this.onlyPreview;
  }

  get disableEditorStatus() {
    return this.onlyPreview;
  }

  @Mutation
  setState(payload: any) {
    Object.assign(this, payload);
  }

  @Mutation
  toggleState(name: SwithNames) {
    if (name === 'onlyPreview') {
      this.onlyEditor = false;
    } else if (name === 'onlyEditor') {
      this.onlyPreview = false;
    }

    this[name] = !this[name];
  }
}
