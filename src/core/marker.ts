import { Diff } from 'diff-match-patch';

const DIFF_DELETE = -1;
const DIFF_INSERT = 1;
const DIFF_EQUAL = 0;

let idCounter = 0;

export interface MarkOption {
  id: string,
  offsetName: string
}

export class Marker {

  id = idCounter;
  offsetName!: string;

  constructor(
    public offset: number,
    public trailing: boolean = false,
  ) {
    idCounter += 1;
  }

  adjustOffset(diffs: Diff[]) {
    let startOffset = 0;
    diffs.forEach(diff => {
      const diffType = diff[0];
      const diffText = diff[1];
      const diffOffset = diffText.length;
      switch (diffType) {
        case DIFF_EQUAL:
          startOffset += diffOffset;
          break;
        case DIFF_INSERT:
          if (
            this.trailing
              ? this.offset > startOffset
              : this.offset >= startOffset
          ) {
            this.offset += diffOffset;
          }
          startOffset += diffOffset;
          break;
        case DIFF_DELETE:
          if (this.offset > startOffset) {
            this.offset -= Math.min(diffOffset, this.offset - startOffset);
          }
          break;
        default:
      }
    });
  }
}
