import { HubItem, ProxyToHub } from '@/core/hub';
import { ArrHasBy } from '@/libs/helpers';
import { isWebkit } from '@/core/utils';
import { Section } from '@/core/section';
import { CORE_HIGHLIGHTED, CORE_SECTION_HIGHLIGHTED } from '@/core/events';

const styleEles: HTMLStyleElement[] = [];

function createStyleSheet(document: Document) {
  const styleEle = document.createElement('style');
  styleEle.type = 'text/css';
  styleEle.innerHTML = '.editor-section * { display: inline; }';
  document.head.appendChild(styleEle);
  styleEles.push(styleEle);
}

export class Highlighter extends ProxyToHub {
  isComposing = 0;
  sectionList: Section[] = [];

  insertBeforeSection?: Section;

  useBr = isWebkit;
  trailingNodeTag = 'div';
  hiddenLfInnerHtml = '<br><span class="hd-lf" style="display: none">\n</span>';
  lfHtml = `<span class="lf">${this.useBr ? this.hiddenLfInnerHtml : '\n'}</span>`;

  cancelComposition: boolean = true;
  trailingNode!: HTMLElement;

  constructor(
    public hub: HubItem,
  ) {
    super(hub);

    if (!ArrHasBy(styleEles, styleEle => document.head.contains(styleEle))) {
      createStyleSheet(document);
    }
  }

  fixContent(modifiedSections: Section[], removedSections: Section[], noContentFix: boolean) {
    modifiedSections.forEach((section: Section) => {
      section.forceHighlighting = true;
      if (!noContentFix) {
        if (this.useBr) {
          Array.prototype.forEach.call(
            section.ele.getElementsByClassName('hd-lf'),
            (lfEle: HTMLElement) => lfEle.parentNode!.removeChild(lfEle),
          );
          Array.prototype.forEach.call(
            section.ele.getElementsByTagName('br'),
            (brEle: HTMLElement) => brEle.parentNode!.replaceChild(document.createTextNode('\n'), brEle),
          );
        }
        if (section.ele.textContent!.slice(-1) !== '\n') {
          section.ele.appendChild(document.createTextNode('\n'));
        }
      }
    });
  }

  addTrailingNode() {
    this.trailingNode = document.createElement(this.trailingNodeTag);
    this.contentEle.appendChild(this.trailingNode);
  }

  parseSections(content: string, isInit: boolean = false) {
    if (this.isComposing && !this.cancelComposition) {
      return this.sectionList;
    }

    this.cancelComposition = false;
    const newSectionList = Array.prototype.map.call(
      (this.editor.options.sectionParser ? this.editor.options.sectionParser(content) : [content]),
      (sectionText: Section) => new Section(sectionText),
    ) as Section[];

    let modifiedSections = [];
    let sectionsToRemove: Section[] = [];
    this.insertBeforeSection = undefined;

    if (isInit) {
      // Render everything if isInit
      sectionsToRemove = this.sectionList;
      this.sectionList = newSectionList;
      modifiedSections = newSectionList;
    } else {
      // Find modified section starting from top
      let leftIndex = this.sectionList.length;
      ArrHasBy(this.sectionList, (section, index) => {
        const newSection = newSectionList[index];
        if (index >= newSectionList.length
          || section.forceHighlighting
          // Check text modification
          || section.text !== newSection.text
          // Check that section has not been detached or moved
          || section.ele.parentNode !== this.contentEle
          // Check also the content since nodes can be injected in sections via copy/paste
          || section.ele.textContent !== newSection.text
        ) {
          leftIndex = index;
          return true;
        }
        return false;
      });

      // Find modified section starting from bottom
      let rightIndex = -this.sectionList.length;
      ArrHasBy(this.sectionList.slice().reverse(), (section, index) => {
        const newSection = newSectionList[newSectionList.length - index - 1];
        if (index >= newSectionList.length
          || section.forceHighlighting
          // Check modified
          || section.text !== newSection.text
          // Check that section has not been detached or moved
          || section.ele.parentNode !== this.contentEle
          // Check also the content since nodes can be injected in sections via copy/paste
          || section.ele.textContent !== newSection.text
        ) {
          rightIndex = -index;
          return true;
        }
        return false;
      });

      if (leftIndex - rightIndex > this.sectionList.length) {
        // Prevent overlap
        rightIndex = leftIndex - this.sectionList.length;
      }

      const leftSections = this.sectionList.slice(0, leftIndex);
      modifiedSections = newSectionList.slice(leftIndex, newSectionList.length + rightIndex);
      const rightSections = this.sectionList.slice(this.sectionList.length + rightIndex, this.sectionList.length);
      [this.insertBeforeSection] = rightSections;
      sectionsToRemove = this.sectionList.slice(leftIndex, this.sectionList.length + rightIndex);
      this.sectionList = leftSections.concat(modifiedSections).concat(rightSections);
    }

    const highlight = (section: Section) => {
      const html = this.editor.options.sectionHighlighter!(section).replace(/\n/g, this.lfHtml);
      const sectionEle = document.createElement('div');
      sectionEle.className = 'editor-section';
      sectionEle.innerHTML = html;
      section.setElement(sectionEle);
      this.$events.$emit(CORE_SECTION_HIGHLIGHTED, section);
    };

    const newSectionEleList = document.createDocumentFragment();
    modifiedSections.forEach(section => {
      section.forceHighlighting = false;
      highlight(section);
      newSectionEleList.appendChild(section.ele);
    });

    this.watcher.noWatch(() => {
      if (isInit) {
        this.contentEle.innerHTML = '';
        this.contentEle.appendChild(newSectionEleList);
        this.addTrailingNode();
        return;
      }

      // Remove outdated sections
      sectionsToRemove.forEach((section: Section) => {
        // section may be already removed
        if (section.ele.parentNode === this.contentEle) {
          this.contentEle.removeChild(section.ele);
        }
        // To detect sections that come back with built-in undo
        section.ele.section = undefined;
      });

      if (this.insertBeforeSection !== undefined) {
        this.contentEle.insertBefore(newSectionEleList, this.insertBeforeSection.ele);
      } else {
        this.contentEle.appendChild(newSectionEleList);
      }

      // Remove unauthorized nodes (text nodes outside of sections or
      // duplicated sections via copy/paste)
      let childNode = this.contentEle.firstChild;
      while (childNode) {
        const nextNode = childNode.nextSibling;
        if (!(childNode as HTMLElement).section) {
          this.contentEle.removeChild(childNode);
        }
        childNode = nextNode;
      }
      this.addTrailingNode();
      this.$events.$emit(CORE_HIGHLIGHTED);

      if (this.selectionMgr.hasFocus()) {
        this.selectionMgr.restoreSelection();
        this.selectionMgr.updateCursorCoordinates();
      }
    });

    return this.sectionList;
  }
}
