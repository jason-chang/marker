
import { HubItem, ProxyToHub } from '@/core/hub';
import { debounce, findContainer } from '@/core/utils';
import { CORE_CURSOR_COORDINATES_CHANGED, CORE_SELECTION_CHANGED } from '@/core/events';

interface RangeContainer {
  container: HTMLElement,
  offsetInContainer: number,
}

interface Coordinates {
  left?: number,
  right?: number,
  top?: number,
  bottom?: number,
  height?: number,
}

export interface SelectionState {
  before: string,
  after: string | string[],
  selection: string,
  isBackwardSelection: boolean,
}

function createSelectionStateSaver(selectionMgr: SelectionMgr) {
  /**
   * Credit: https://github.com/timdown/rangy
   */
  function arrayContains<T>(arr: Array<T>, val: any) {
    let i = arr.length;
    while (i) {
      i -= 1;
      if (arr[i] === val) {
        return true;
      }
    }
    return false;
  }

  function getClosestAncestorIn(node: Node, ancestor: Node, selfIsAncestor: boolean) {
    let p;
    let n = selfIsAncestor ? node : node.parentNode;
    while (n) {
      p = n.parentNode;
      if (p === ancestor) {
        return n;
      }
      n = p;
    }
    return null;
  }

  function getNodeIndex(node: Node) {
    let i = 0;
    let { previousSibling } = node;
    while (previousSibling) {
      i += 1;
      ({ previousSibling } = previousSibling);
    }
    return i;
  }

  function getCommonAncestor(node1: Node, node2: Node) {
    const ancestors = [];
    let n;
    for (n = node1; n; n = n.parentNode) {
      ancestors.push(n);
    }

    for (n = node2; n; n = n.parentNode) {
      if (arrayContains(ancestors, n)) {
        return n;
      }
    }

    return null;
  }

  function comparePoints(nodeA: Node, offsetA: number, nodeB: Node, offsetB: number) {
    // See http://www.w3.org/TR/DOM-Level-2-Traversal-Range/ranges.html#Level-2-Range-Comparing
    let n;
    if (nodeA === nodeB) {
      // Case 1: nodes are the same
      if (offsetA === offsetB) {
        return 0;
      }
      return offsetA < offsetB ? -1 : 1;
    }
    let nodeC = getClosestAncestorIn(nodeB, nodeA, true);
    if (nodeC) {
      // Case 2: node C (container B or an ancestor) is a child node of A
      return offsetA <= getNodeIndex(nodeC) ? -1 : 1;
    }
    nodeC = getClosestAncestorIn(nodeA, nodeB, true);
    if (nodeC) {
      // Case 3: node C (container A or an ancestor) is a child node of B
      return getNodeIndex(nodeC) < offsetB ? -1 : 1;
    }
    const root = getCommonAncestor(nodeA, nodeB);
    if (!root) {
      throw new Error('comparePoints error: nodes have no common ancestor');
    }

    // Case 4: containers are siblings or descendants of siblings
    const childA = (nodeA === root) ? root : getClosestAncestorIn(nodeA, root, true);
    const childB = (nodeB === root) ? root : getClosestAncestorIn(nodeB, root, true);

    if (childA === childB) {
      // This shouldn't be possible
      throw new Error('comparePoints got to case 4 and childA and childB are the same!');
    }

    n = root.firstChild;
    while (n) {
      if (n === childA) {
        return -1;
      } if (n === childB) {
        return 1;
      }
      n = n.nextSibling;
    }
    return 0;
  }

  function save(): boolean {
    if (!selectionMgr.hasFocus()) {
      return false;
    }

    let result;

    let { selectionStart, selectionEnd } = selectionMgr;
    const selection = window.getSelection()!;
    if (selection.rangeCount > 0) {
      const selectionRange = selection.getRangeAt(0);
      let node = selectionRange.startContainer;
      // eslint-disable-next-line no-bitwise
      if ((selectionMgr.contentEle.compareDocumentPosition(node)
        & Node.DOCUMENT_POSITION_CONTAINED_BY)
        || selectionMgr.contentEle === node
      ) {
        let offset = selectionRange.startOffset;
        if (node.firstChild && offset > 0) {
          node = node.childNodes[offset - 1];
          offset = node.textContent!.length;
        }
        let container = node;
        while (node !== selectionMgr.contentEle) {
          node = node.previousSibling!;
          while (node) {
            offset += (node.textContent || '').length;
            node = node.previousSibling!;
          }
          node = container.parentNode!;
          container = node;
        }
        let selectionText = `${selectionRange}`;
        // Fix end of line when only br is selected
        const brEle = selectionRange.endContainer.firstChild as HTMLElement;
        if (brEle && brEle.tagName === 'BR' && selectionRange.endOffset === 1) {
          selectionText += '\n';
        }
        if (comparePoints(
          selection.anchorNode!,
          selection.anchorOffset!,
          selection.focusNode!,
          selection.focusOffset!,
        ) === 1) {
          selectionStart = offset + selectionText.length;
          selectionEnd = offset;
        } else {
          selectionStart = offset;
          selectionEnd = offset + selectionText.length;
        }

        if (selectionStart === selectionEnd && selectionStart === selectionMgr.editor.getContent().length) {
          // If cursor is after the trailingNode
          selectionEnd -= 1;
          selectionStart = selectionEnd;
          result = selectionMgr.setSelectionStartEnd(selectionStart, selectionEnd);
        } else {
          selectionMgr.setSelection(selectionStart, selectionEnd);
          result = selectionMgr.checkSelection(selectionRange);
          // selectionRange doesn't change when selection is at the start of a section
          result = result || selectionMgr.lastSelectionStart !== selectionMgr.selectionStart;
        }
      }
    }

    return !!result;
  }

  function saveCheckChange(): boolean {
    return save() && (selectionMgr.lastSelectionStart !== selectionMgr.selectionStart || selectionMgr.lastSelectionEnd !== selectionMgr.selectionEnd);
  }

  let nextTickAdjustScroll = false;

  const longerDebouncedSave = debounce(() => {
    selectionMgr.updateCursorCoordinates(saveCheckChange() && nextTickAdjustScroll);
    nextTickAdjustScroll = false;
  }, 10);

  const debouncedSave = debounce(() => {
    selectionMgr.updateCursorCoordinates(saveCheckChange() && nextTickAdjustScroll);
    // In some cases we have to wait a little longer to see the
    // selection change (Cmd+A on Chrome OSX)
    longerDebouncedSave();
  });

  return (debounced: boolean, adjustScrollParam: boolean, forceAdjustScroll: boolean) => {
    if (forceAdjustScroll) {
      selectionMgr.lastSelectionStart = 0;
      selectionMgr.lastSelectionEnd = 0;
    }
    if (debounced) {
      nextTickAdjustScroll = nextTickAdjustScroll || adjustScrollParam;
      debouncedSave();
    } else {
      save();
    }
  };
}

export class SelectionMgr extends ProxyToHub {
  lastSelectionStart = 0;
  lastSelectionEnd = 0;
  selectionStart = 0;
  selectionEnd = 0;

  selectionEndContainer: HTMLElement | null = null;
  selectionEndOffset = 0;
  oldSelectionRange!: Range;
  cursorCoordinates: Coordinates = {};

  debouncedUpdateCursorCoordinates: Function;
  saveLastSelection: Function;
  saveSelectionState: Function;

  adjustScroll = false;

  constructor(
    public hub: HubItem,
  ) {
    super(hub);

    this.debouncedUpdateCursorCoordinates = debounce(this.updateCursorCoordinatesWorker.bind(this));

    this.saveLastSelection = debounce(() => {
      this.lastSelectionStart = this.selectionStart;
      this.lastSelectionEnd = this.selectionEnd;
    }, 50);

    this.saveSelectionState = createSelectionStateSaver(this);
  }

  findContainer(offset: number) {
    const result = findContainer(this.contentEle, offset);
    if (result.container.nodeValue === '\n') {
      const hdLfEle = result.container.parentNode! as HTMLElement;
      if (hdLfEle.className === 'hd-lf' && hdLfEle.previousSibling && (hdLfEle.previousSibling! as HTMLElement).tagName === 'BR') {
        result.container = hdLfEle.parentNode! as HTMLElement;
        result.offsetInContainer = Array.prototype.indexOf.call(
          result.container.childNodes,
          result.offsetInContainer === 0 ? hdLfEle.previousSibling : hdLfEle,
        );
      }
    }
    return result;
  }

  createRange(start: RangeContainer | number, end: RangeContainer | number) {
    const range = document.createRange();
    const startContainer = typeof start === 'number'
      ? this.findContainer(start < 0 ? 0 : start)
      : start;
    let endContainer = startContainer;
    if (start !== end) {
      endContainer = typeof end === 'number'
        ? this.findContainer(end < 0 ? 0 : end)
        : end;
    }
    range.setStart(startContainer.container, startContainer.offsetInContainer);
    range.setEnd(endContainer.container, endContainer.offsetInContainer);
    return range;
  }

  updateCursorCoordinatesWorker() {
    const coordinates = this.getCoordinates(
      this.selectionEnd,
      this.selectionEndContainer,
      this.selectionEndOffset,
    );

    if (this.cursorCoordinates.top !== coordinates.top
      || this.cursorCoordinates.height !== coordinates.height
      || this.cursorCoordinates.left !== coordinates.left
    ) {
      this.cursorCoordinates = coordinates;
      this.$events.$emit(CORE_CURSOR_COORDINATES_CHANGED, coordinates);
    }

    if (!this.adjustScroll) {
      return;
    }

    let scrollEleHeight = this.scrollEle.clientHeight;
    if (typeof this.adjustScroll === 'number') {
      scrollEleHeight -= this.adjustScroll;
    }

    const adjustment = (scrollEleHeight / 2) * this.editor.options.getCursorFocusRatio!();
    let cursorTop = this.cursorCoordinates.top! + (this.cursorCoordinates.height! / 2);
    // Adjust cursorTop with contentEle position relative to scrollEle
    cursorTop += (this.contentEle.getBoundingClientRect().top - this.scrollEle.getBoundingClientRect().top)
      + this.scrollEle.scrollTop;

    const minScrollTop = cursorTop - adjustment;
    const maxScrollTop = (cursorTop + adjustment) - scrollEleHeight;

    if (this.scrollEle.scrollTop > minScrollTop) {
      this.scrollEle.scrollTop = minScrollTop;
    } else if (this.scrollEle.scrollTop < maxScrollTop) {
      this.scrollEle.scrollTop = maxScrollTop;
    }
    this.adjustScroll = false;
  }

  updateCursorCoordinates(adjustScrollParam: boolean = false) {
    this.adjustScroll = this.adjustScroll || adjustScrollParam;
    this.debouncedUpdateCursorCoordinates();
  }

  checkSelection(selectionRange: Range) {
    if (!this.oldSelectionRange
      || this.oldSelectionRange.startContainer !== selectionRange.startContainer
      || this.oldSelectionRange.startOffset !== selectionRange.startOffset
      || this.oldSelectionRange.endContainer !== selectionRange.endContainer
      || this.oldSelectionRange.endOffset !== selectionRange.endOffset
    ) {
      this.oldSelectionRange = selectionRange;
      this.$events.$emit(CORE_SELECTION_CHANGED, this.selectionStart, this.selectionEnd, selectionRange);
      return true;
    }
    return false;
  }

  hasFocus() {
    return this.contentEle === document.activeElement;
  }

  restoreSelection() {
    const min = Math.min(this.selectionStart, this.selectionEnd);
    const max = Math.max(this.selectionStart, this.selectionEnd);
    const selectionRange = this.createRange(min, max);
    if (!document.contains(selectionRange.commonAncestorContainer)) {
      return null;
    }
    const selection = window.getSelection()!;
    selection.removeAllRanges();
    const isBackward = this.selectionStart > this.selectionEnd;
    if (isBackward && selection.extend) {
      const beginRange = selectionRange.cloneRange();
      beginRange.collapse(false);
      selection.addRange(beginRange);
      selection.extend(selectionRange.startContainer, selectionRange.startOffset);
    } else {
      selection.addRange(selectionRange);
    }
    this.checkSelection(selectionRange);
    return selectionRange;
  }

  setSelection(start = this.selectionStart, end = this.selectionEnd) {
    this.selectionStart = start < 0 ? 0 : start;
    this.selectionEnd = end < 0 ? 0 : end;
    this.saveLastSelection();
  }

  setSelectionStartEnd(start: number, end: number, restoreSelection = true) {
    this.setSelection(start, end);
    if (restoreSelection && this.hasFocus()) {
      return this.restoreSelection();
    }
    return null;
  }

  getSelectedText() {
    const min = Math.min(this.selectionStart, this.selectionEnd);
    const max = Math.max(this.selectionStart, this.selectionEnd);
    return this.editor.getContent().substring(min, max);
  }

  getCoordinates(inputOffset: number, containerParam: HTMLElement | null, offsetInContainerParam: number) {
    let container = containerParam;
    let offsetInContainer = offsetInContainerParam;
    if (!container) {
      const offset = this.findContainer(inputOffset);
      ({ container } = offset);
      ({ offsetInContainer } = offset);
    }
    let containerEle = container;
    if (!containerEle.hasChildNodes() && container.parentNode) {
      containerEle = container.parentNode as HTMLElement;
    }
    let isInvisible = false;
    while (!containerEle.offsetHeight) {
      isInvisible = true;
      if (containerEle.previousSibling) {
        containerEle = containerEle.previousSibling as HTMLElement;
      } else {
        containerEle = containerEle.parentNode as HTMLElement;
        if (!containerEle) {
          return {
            top: 0,
            height: 0,
            left: 0,
          };
        }
      }
    }

    let rect;
    let offset = 0;
    if (isInvisible || container.textContent === '\n') {
      rect = containerEle.getBoundingClientRect();
      offset = rect.left;
    } else {
      const selectedChar = this.editor.getContent()[inputOffset];

      const startOffset = { container, offsetInContainer };
      const endOffset = { container, offsetInContainer };

      if (inputOffset > 0 && (selectedChar === undefined || selectedChar === '\n')) {
        if (startOffset.offsetInContainer === 0) {
          // Need to calculate offset-1
          startOffset.offsetInContainer = inputOffset - 1;
        } else {
          startOffset.offsetInContainer -= 1;
        }
      } else if (endOffset.offsetInContainer === container.textContent!.length) {
        // Need to calculate offset+1
        endOffset.offsetInContainer = inputOffset + 1;
      } else {
        endOffset.offsetInContainer += 1;
      }
      const range = this.createRange(startOffset, endOffset);
      rect = range.getBoundingClientRect();
      offset = rect.right;
    }

    const contentRect = this.contentEle.getBoundingClientRect();

    return {
      top: Math.round((rect.top - contentRect.top) + this.contentEle.scrollTop),
      height: Math.round(rect.height),
      left: Math.round((offset - contentRect.left) + this.contentEle.scrollLeft),
    };
  }
}
