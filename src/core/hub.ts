import Vue from 'vue';
import { Editor } from '@/core/editor';
import { UndoMgr } from '@/core/undoMgr';
import { SelectionMgr } from '@/core/selectionMgr';
import { Highlighter } from '@/core/highlighter';
import { Watcher } from '@/core/watcher';
import { Settings } from '@/conf/settings';

export interface HubItem {
  settings?: Settings,
  $events?: Vue,
  editor?: Editor,
  undoMgr?: UndoMgr,
  selectionMgr?: SelectionMgr,
  highlighter?: Highlighter,
  watcher?: Watcher
}

export class ProxyToHub {

  constructor(
    public hub: HubItem,
  ) {
    // not empty
  }

  get $events() :Vue {
    return this.hub.$events!;
  }

  get settings() :Settings {
    return this.hub.settings!;
  }

  get editor() :Editor {
    return this.hub.editor!;
  }

  get undoMgr() :UndoMgr {
    return this.hub.undoMgr!;
  }

  get selectionMgr() :SelectionMgr {
    return this.hub.selectionMgr!;
  }

  get highlighter() :Highlighter {
    return this.hub.highlighter!;
  }

  get watcher() :Watcher {
    return this.hub.watcher!;
  }

  get contentEle() :HTMLElement {
    return this.editor.$contentEle;
  }

  get scrollEle() :HTMLElement {
    return this.editor.$scrollEle;
  }

}
