import { create as creater } from './create';
import { SelectionMgr as SelectionMgrTmp, SelectionState as SelectionStateTmp } from '@/core/selectionMgr';

export { Editor } from '@/core/editor';
export { Highlighter } from '@/core/highlighter';
export { Marker } from '@/core/marker';
export { UndoMgr } from '@/core/undoMgr';
export { Watcher } from '@/core/watcher';
export { Keystroke, defaultKeystrokes } from '@/core/keystroke';

export const SelectionMgr = SelectionMgrTmp;
export interface SelectionState extends SelectionStateTmp {}

export default creater;
