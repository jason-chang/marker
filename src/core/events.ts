
export const CORE_UNDO_STATE_CHANGE = 'UNDO_STATE_CHANGE';
export const CORE_CURSOR_COORDINATES_CHANGED = 'CURSOR_COORDINATES_CHANGED';
export const CORE_SELECTION_CHANGED = 'SELECTION_CHANGED';
export const CORE_SECTION_HIGHLIGHTED = 'SECTION_HIGHLIGHTED';
export const CORE_HIGHLIGHTED = 'HIGHLIGHTED';
export const CORE_CONTENT_CHANGED = 'EDITOR_CONTENT_CHANGED';
export const CORE_DESTROY = 'EDITOR_DESTROY';
export const CORE_FOCUS = 'EDITOR_FOCUS';
export const CORE_BLUR = 'EDITOR_BLUR';

export class Events {
  listenerMap: {
    [prop: string]: Array<Function>
  } = {};

  trigger(eventType: string, ...args: any[] | []) {
    const listeners = this.listenerMap[eventType];
    if (listeners) {
      listeners.forEach(listener => {
        try {
          listener.apply(this, args);
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e.message, e.stack);
        }
      });
    }
  }

  on(eventType: string, listener: Function) {
    let listeners = this.listenerMap[eventType];
    if (!listeners) {
      listeners = [];
      this.listenerMap[eventType] = listeners;
    }
    listeners.push(listener);
  }

  off(eventType: string, listener: Function) {
    const listeners = this.listenerMap[eventType];
    if (listeners) {
      const index = listeners.indexOf(listener);
      if (index !== -1) {
        listeners.splice(index, 1);
      }
    }
  }
}
