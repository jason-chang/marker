import Vue from 'vue';
import { Editor } from '@/core/editor';
import { HubItem } from '@/core/hub';
import { UndoMgr } from '@/core/undoMgr';
import { SelectionMgr } from '@/core/selectionMgr';
import { Watcher } from '@/core/watcher';
import { Highlighter } from '@/core/highlighter';
import { Settings } from '@/conf/settings';

export function create(events: Vue, contentEle: HTMLElement, scrollEle: HTMLElement, settings: Settings, isMarkdown = false) {
  const hubItem: HubItem = {};
  hubItem.settings = settings;
  hubItem.$events = events;
  hubItem.undoMgr = new UndoMgr(hubItem);
  hubItem.selectionMgr = new SelectionMgr(hubItem);
  hubItem.highlighter = new Highlighter(hubItem);

  const editor = new Editor(hubItem, contentEle, scrollEle, isMarkdown);
  hubItem.editor = editor;
  const watcher = new Watcher(hubItem, editor.checkContentChange.bind(editor));
  watcher.startWatching();
  hubItem.watcher = watcher;

  return editor;
}
