/* eslint-disable camelcase */
import { Diff, diff_match_patch, patch_obj } from 'diff-match-patch';
import { HubItem, ProxyToHub } from '@/core/hub';
import { debounce } from '@/core/utils';
import { CORE_UNDO_STATE_CHANGE } from '@/core/events';

// eslint-disable-next-line new-cap
const diffMatchPatch = new diff_match_patch();

class StateMgr {
  currentTime = 0;
  lastTime = 0;
  lastMode = '';
  currentMode = 'single';

  constructor(
    public undoMgr: UndoMgr,
  ) {
    // not empty
  }

  isBufferState() {
    this.currentTime = Date.now();
    return this.currentMode !== 'single'
      && this.currentMode === this.lastMode
      && this.currentTime - this.lastTime < this.undoMgr.options.bufferStateUntilIdle;
  }

  setDefaultMode(mode: string = '') {
    this.currentMode = this.currentMode || mode;
  }

  resetMode() {
    this.currentMode = '';
    this.lastMode = '';
  }

  saveMode() {
    this.lastMode = this.currentMode;
    this.currentMode = '';
    this.lastTime = this.currentTime;
  }
}

class State {
  // eslint-disable-next-line camelcase
  patches: patch_obj[] = [];

  constructor(public undoMgr: UndoMgr) {
    // not empty
  }

  addToUndoStack() {
    this.undoMgr.undoStack.push(this);
    this.patches = this.undoMgr.previousPatches;
    this.undoMgr.previousPatches = [];
  }

  addToRedoStack() {
    this.undoMgr.redoStack.push(this);
    this.patches = this.undoMgr.previousPatches;
    this.undoMgr.previousPatches = [];
  }
}

export class UndoMgr extends ProxyToHub {

  undoStack: State[] = [];
  redoStack: State[] = [];

  currentState!: State;
  previousPatches: patch_obj[] = [];
  currentPatches: patch_obj[] = [];

  options = {
    undoStackMaxSize: 200,
    bufferStateUntilIdle: 1000,
    patchHandler: {

      makePatches(
        oldContent: string,
        newContent: string,
        // eslint-disable-next-line camelcase
        diffs: Diff[],
      ) {
        return diffMatchPatch.patch_make(oldContent, diffs);
      },
      applyPatches(
        // eslint-disable-next-line camelcase
        patches: patch_obj[],
        content: string,
      ) {
        return diffMatchPatch.patch_apply(patches, content)[0];
      },
      reversePatches(
        // eslint-disable-next-line camelcase
        patches: patch_obj[],
      ) {
        const reversedPatches = diffMatchPatch.patch_deepCopy(patches).reverse();
        reversedPatches.forEach((
          // eslint-disable-next-line camelcase
          patch: patch_obj,
        ) => {
          patch.diffs.forEach(diff => {
            diff[0] = -diff[0];
          });
        });
        return reversedPatches;
      },
    },
  };

  stateMgr: StateMgr;

  saveState: Function;

  constructor(
    public hub: HubItem,
  ) {
    super(hub);
    // not empty
    this.stateMgr = new StateMgr(this);

    this.saveState = debounce(this.saveStateWorker.bind(this));
  }

  setCurrentMode(mode: string) {
    this.stateMgr.currentMode = mode;
  }

  setDefaultMode(mode: string) {
    this.stateMgr.setDefaultMode(mode);
  }

  addDiffs(oldContent: string, newContent: string, diffs: Diff[]) {
    const patches = this.options.patchHandler.makePatches(oldContent, newContent, diffs);
    // eslint-disable-next-line camelcase
    patches.forEach((patch: patch_obj) => this.currentPatches.push(patch));
  }

  saveCurrentPatches() {
    // Move currentPatches into previousPatches
    Array.prototype.push.apply(this.previousPatches, this.currentPatches);
    this.currentPatches = [];
  }

  saveStateWorker() {
    this.redoStack.length = 0;
    if (!this.stateMgr.isBufferState()) {
      this.currentState.addToUndoStack();

      // Limit the size of the stack
      while (this.undoStack.length > this.options.undoStackMaxSize) {
        this.undoStack.shift();
      }
    }
    this.saveCurrentPatches();
    this.currentState = new State(this);
    this.stateMgr.saveMode();
    this.$events.$emit(CORE_UNDO_STATE_CHANGE);
  }

  canUndo() {
    return !!this.undoStack.length;
  }

  canRedo() {
    return !!this.redoStack.length;
  }

  // eslint-disable-next-line camelcase
  restoreState(patchesParam: patch_obj[], isForward: boolean = false) {
    let patches = patchesParam;
    // Update editor
    const content = this.editor.getContent();
    if (!isForward) {
      patches = this.options.patchHandler.reversePatches(patches);
    }

    const newContentText = this.options.patchHandler.applyPatches(patches, content);
    const range = this.editor.setContent(newContentText, true);
    const selection = { start: range.end, end: range.end };

    this.selectionMgr.setSelectionStartEnd(selection.start, selection.end);
    this.selectionMgr.updateCursorCoordinates(true);

    this.stateMgr.resetMode();
    this.$events.$emit(CORE_UNDO_STATE_CHANGE);
    this.editor.adjustCursorPosition();
  }

  undo() {
    const state = this.undoStack.pop();

    if (!state) return;

    this.saveCurrentPatches();
    this.currentState.addToRedoStack();
    this.restoreState(this.currentState.patches);
    this.previousPatches = state.patches;
    this.currentState = state;
  }

  redo() {
    const state = this.redoStack.pop();
    if (!state) {
      return;
    }
    this.currentState.addToUndoStack();
    this.restoreState(state.patches, true);
    this.previousPatches = state.patches;
    this.currentState = state;
  }

  init(options = {}) {
    this.options = Object.assign(this.options, options);

    if (!this.currentState) {
      this.currentState = new State(this);
    }
  }
}
