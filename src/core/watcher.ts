
import { HubItem, ProxyToHub } from '@/core/hub';

export class Watcher extends ProxyToHub {
  isWatching = false;
  contentObserver?: MutationObserver;

  constructor(
    public hub: HubItem,
    public listener: MutationCallback,
  ) {
    super(hub);
  }

  startWatching() {
    this.stopWatching();
    this.isWatching = true;
    this.contentObserver = new MutationObserver(this.listener);
    this.contentObserver!.observe(this.contentEle, {
      childList: true,
      subtree: true,
      characterData: true,
    });
  }

  stopWatching() {
    if (this.contentObserver) {
      this.contentObserver.disconnect();
      this.contentObserver = undefined;
    }
    this.isWatching = false;
  }

  noWatch(cb: Function) {
    if (this.isWatching) {
      this.stopWatching();
      cb();
      this.startWatching();
    } else {
      cb();
    }
  }
}
