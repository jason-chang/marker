
export class Section {
  text = '';
  data: any = {};
  forceHighlighting = false;
  ele!: HTMLElement;

  constructor(text: Section | string) {
    if (typeof text === 'string') {
      this.text = text;
    } else {
      this.text = text.text;
      this.data = text.data;
    }
  }

  setElement(ele: HTMLElement) {
    this.ele = ele;
    ele.section = this;
  }
}
