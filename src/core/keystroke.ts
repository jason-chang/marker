
import { SelectionState } from '@/core/selectionMgr';
import { Editor } from '@/core/editor';
import { isMac } from '@/core/utils';

let clearNewline: boolean;

const charTypes: { [key: string]: string } = { ' ': 'space', '\t': 'space', '\n': 'newLine' };

// Word separators, as in Sublime Text
'./\\()"\'-:,.;<>~!@#$%^&*|+=[]{}`~?'.split('').forEach(wordSeparator => {
  charTypes[wordSeparator] = 'wordSeparator';
});

function getNextWordOffset(text: string, offset: number, isBackward = false) {
  let previousType;
  let result = offset;
  while ((isBackward && result > 0) || (!isBackward && result < text.length)) {
    const currentType = charTypes[isBackward ? text[result - 1] : text[result]] || 'word';
    if (previousType && currentType !== previousType) {
      if (previousType === 'word' || currentType === 'space' || previousType === 'newLine' || currentType === 'newLine') {
        break;
      }
    }
    previousType = currentType;
    if (isBackward) {
      result -= 1;
    } else {
      result += 1;
    }
  }
  return result;
}

export class Keystroke {
  constructor(
    public handler: Function,
    public priority = 100,
  ) {
    // not empty
  }
}

export const defaultKeystrokes = [
  // Command + y && Command + z
  new Keystroke((event: KeyboardEvent, state: SelectionState, editor: Editor) => {
    if ((!event.ctrlKey && !event.metaKey) || event.altKey) {
      return false;
    }
    const keyCode = event.charCode || event.keyCode;
    const keyCodeChar = String.fromCharCode(keyCode).toLowerCase();
    let action;
    switch (keyCodeChar) {
      case 'y':
        event.preventDefault();
        setTimeout(() => editor.undoMgr.redo(), 10);
        break;
      case 'z':
        event.preventDefault();

        setTimeout(
          () => (event.shiftKey ? editor.undoMgr.redo() : editor.undoMgr.undo()),
          10,
        );
        break;
      default:
        return false;
    }
    return true;
  }),

  // handle type tab key
  new Keystroke((event: KeyboardEvent, state: SelectionState) => {
    if (event.which !== 9 /* tab */ || event.metaKey || event.ctrlKey) {
      return false;
    }

    const strSplice = (str: string, i: number, remove: number, add = '') => str.slice(0, i) + add + str.slice(i + (+remove || 0));

    event.preventDefault();
    const isInverse = event.shiftKey;
    const lf = state.before.lastIndexOf('\n') + 1;
    if (isInverse) {
      if (/\s/.test(state.before.charAt(lf))) {
        state.before = strSplice(state.before, lf, 1);
      }
      state.selection = state.selection.replace(/^[ \t]/gm, '');
    } else if (state.selection) {
      state.before = strSplice(state.before, lf, 0, '\t');
      state.selection = state.selection.replace(/\n(?=[\s\S])/g, '\n\t');
    } else {
      state.before += '\t';
    }
    return true;
  }),

  // Handle type enter key.
  new Keystroke((event: KeyboardEvent, state: SelectionState, editor: Editor) => {
    if (event.which !== 13 /* enter */) {
      clearNewline = false;
      return false;
    }

    event.preventDefault();
    const lf = state.before.lastIndexOf('\n') + 1;
    if (clearNewline) {
      state.before = state.before.substring(0, lf);
      state.selection = '';
      clearNewline = false;
      return true;
    }
    clearNewline = false;
    const previousLine = state.before.slice(lf);
    const indent = previousLine.match(/^\s*/)![0];
    if (indent.length) {
      clearNewline = true;
    }

    editor.undoMgr.setCurrentMode('single');
    state.before += `\n${indent}`;
    state.selection = '';
    return true;
  }),

  // Handle type backspace key.
  new Keystroke((event: KeyboardEvent, state: SelectionState, editor: Editor) => {
    if (event.which !== 8 /* backspace */ && event.which !== 46 /* delete */) {
      return false;
    }

    editor.undoMgr.setCurrentMode('delete');
    if (!state.selection) {
      const isJump = (isMac && event.altKey) || (!isMac && event.ctrlKey);
      if (isJump) {
        // Custom kill word behavior
        const text = state.before + state.after;
        const offset = getNextWordOffset(text, state.before.length, event.which === 8);
        if (event.which === 8) {
          state.before = state.before.slice(0, offset);
        } else {
          state.after = state.after.slice(offset - text.length);
        }
        event.preventDefault();
        return true;
      } if (event.which === 8 && state.before.slice(-1) === '\n') {
        // Special treatment for end of lines
        state.before = state.before.slice(0, -1);
        event.preventDefault();
        return true;
      } if (event.which === 46 && state.after.slice(0, 1) === '\n') {
        state.after = state.after.slice(1);
        event.preventDefault();
        return true;
      }
    } else {
      state.selection = '';
      event.preventDefault();
      return true;
    }
    return false;
  }),

  // handle type left/right arrow.
  new Keystroke((event: KeyboardEvent, state: SelectionState, editor: Editor) => {
    if (event.which !== 37 /* left arrow */ && event.which !== 39 /* right arrow */) {
      return false;
    }
    const isJump = (isMac && event.altKey) || (!isMac && event.ctrlKey);
    if (!isJump) {
      return false;
    }

    // Custom jump behavior
    const textContent = editor.getContent();
    const offset = getNextWordOffset(
      textContent,
      editor.selectionMgr.selectionEnd,
      event.which === 37,
    );
    if (event.shiftKey) {
      // rebuild the state completely
      const min = Math.min(editor.selectionMgr.selectionStart, offset);
      const max = Math.max(editor.selectionMgr.selectionStart, offset);
      state.before = textContent.slice(0, min);
      state.after = textContent.slice(max);
      state.selection = textContent.slice(min, max);
      state.isBackwardSelection = editor.selectionMgr.selectionStart > offset;
    } else {
      state.before = textContent.slice(0, offset);
      state.after = textContent.slice(offset);
      state.selection = '';
    }
    event.preventDefault();
    return true;
  }),
];
