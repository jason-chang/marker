/* eslint-disable camelcase, no-underscore-dangle */
import { diff_match_patch } from 'diff-match-patch';
import TurndownService from 'turndown';
import sanitizeHtml from 'sanitize-html';

import { HubItem, ProxyToHub } from '@/core/hub';
import { debounce } from '@/core/utils';
import { ArrHasBy, functionBindWith } from '@/libs/helpers';
import { Section } from '@/core/section';
import { CORE_CONTENT_CHANGED, CORE_BLUR, CORE_DESTROY, CORE_FOCUS } from '@/core/events';
import { Marker } from '@/core/marker';
import { defaultKeystrokes, Keystroke } from '@/core/keystroke';
import { SelectionState } from '@/core/selectionMgr';

// eslint-disable-next-line camelcase, new-cap
const diffMatchPatch = new diff_match_patch();

export interface EditorOptions {
  content?: string,
  getCursorFocusRatio?: () => number,
  sectionHighlighter?: (section: Section) => string,
  sectionDelimiter?: string,
  sectionParser?: Function,
  selectionStart?: number,
  selectionEnd?: number,
  scrollTop?: number,
}

export class Editor extends ProxyToHub {
  static _turndownService: TurndownService;

  keystrokes: Keystroke[] = [];
  markers: Marker[] = [];
  ignoreUndo = false;
  noContentFix = false;
  options!: EditorOptions;

  skipSaveSelection: boolean = false;
  lastTextContent: string;
  triggerSpellCheck: Function;

  get turndownService() {
    if (Editor._turndownService) {
      return Editor._turndownService;
    }

    const turndownService = new TurndownService(this.settings.turndown);
    turndownService.escape = str => str; // Disable escaping
    Editor._turndownService = turndownService;
    return turndownService;
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(
    public hub: HubItem,
    public $contentEle: HTMLElement,
    public $scrollEle: HTMLElement,
    public isMarkdown: boolean = false,
  ) {
    super(hub);
    $contentEle.setAttribute('tabindex', '0'); // To have focus even when disabled
    this.toggleEditable(true);
    this.lastTextContent = this.getTextContent();


    this.triggerSpellCheck = debounce(this.triggerSpellCheckWorker.bind(this), 10);

    this.listenWindowEvent();
    this.listenContentEleEvent();

    this.addKeystroke(defaultKeystrokes);
  }

  init(opts: EditorOptions = {}) {
    const defaultOptions = {
      getCursorFocusRatio() {
        return 0.1;
      },
      sectionHighlighter(section: Section) {
        return section.text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/\u00a0/g, ' ');
      },
      sectionDelimiter: '',
    };

    const options: EditorOptions = Object.assign({}, defaultOptions, opts);
    this.options = options;

    if (options.content !== undefined) {
      this.lastTextContent = options.content.toString();
      if (this.lastTextContent.slice(-1) !== '\n') {
        this.lastTextContent += '\n';
      }
    }

    const sectionList = this.highlighter.parseSections(this.lastTextContent, true);
    this.$events.$emit(CORE_CONTENT_CHANGED, this.lastTextContent, [0, this.lastTextContent], sectionList);
    if (this.options.selectionStart !== undefined && options.selectionEnd !== undefined) {
      this.setSelection(options.selectionStart!, options.selectionEnd!);
    } else {
      this.selectionMgr.saveSelectionState();
    }

    this.undoMgr.init(options);

    if (options.scrollTop !== undefined) {
      this.$scrollEle.scrollTop = options.scrollTop;
    }
  }

  toggleEditable(isEditable: string | boolean | undefined) {
    let editable = isEditable;
    if (isEditable === undefined) {
      editable = this.$contentEle.contentEditable.length
        ? this.$contentEle.contentEditable
        : '';
    }
    this.$contentEle.contentEditable = editable!.toString();
  }

  getTextContent() {
    // Markdown-it sanitization (Mac/DOS to Unix)
    let textContent = this.$contentEle.textContent!.replace(/\r[\n\u0085]?|[\u2424\u2028\u0085]/g, '\n');
    if (textContent.slice(-1) !== '\n') {
      textContent += '\n';
    }
    return textContent;
  }

  getContent() {
    return this.getTextContent();
  }

  adjustCursorPosition(force: boolean = false) {
    this.selectionMgr.saveSelectionState(true, true, force);
  }

  replaceContent(selectionStart: number, selectionEnd: number, replacement: string) {
    const min = Math.min(selectionStart, selectionEnd);
    const max = Math.max(selectionStart, selectionEnd);
    const range = this.selectionMgr.createRange(min, max);
    const rangeText = `${range}`;
    // Range can contain a br element, which is not taken into account in rangeText
    if (rangeText.length === max - min && rangeText === replacement) {
      return null;
    }
    range.deleteContents();
    range.insertNode(document.createTextNode(replacement));
    return range;
  }

  setContent(value: string, noUndo: boolean, maxStartOffsetOpt?: number) {
    const textContent = this.getTextContent();
    const maxStartOffset = maxStartOffsetOpt !== undefined && maxStartOffsetOpt < textContent.length
      ? maxStartOffsetOpt
      : textContent.length - 1;
    const startOffset = Math.min(
      diffMatchPatch.diff_commonPrefix(textContent, value),
      maxStartOffset,
    );
    const endOffset = Math.min(
      diffMatchPatch.diff_commonSuffix(textContent, value),
      textContent.length - startOffset,
      value.length - startOffset,
    );
    const replacement = value.substring(startOffset, value.length - endOffset);
    const range = this.replaceContent(startOffset, textContent.length - endOffset, replacement);
    if (range) {
      this.ignoreUndo = noUndo;
      this.noContentFix = true;
    }
    return {
      start: startOffset,
      end: value.length - endOffset,
      range,
    };
  }

  replaceAt(selectionStart: number, selectionEnd: number, replacement: string) {
    this.undoMgr.setDefaultMode('single');
    this.replaceContent(selectionStart, selectionEnd, replacement);
    const startOffset = Math.min(selectionStart, selectionEnd);
    const endOffset = startOffset + replacement.length;
    this.selectionMgr.setSelectionStartEnd(endOffset, endOffset);
    this.selectionMgr.updateCursorCoordinates(true);
  }

  replaceAll(search: string | RegExp, replacement: string, startOffset = 0) {
    this.undoMgr.setDefaultMode('single');
    const text = this.getTextContent();
    const subtext = this.getTextContent().slice(startOffset);
    const value = subtext.replace(search, replacement);
    if (value !== subtext) {
      const offset = this.setContent(text.slice(0, startOffset) + value, false);
      this.selectionMgr.setSelectionStartEnd(offset.end, offset.end);
      this.selectionMgr.updateCursorCoordinates(true);
    }
  }

  focus() {
    this.selectionMgr.restoreSelection();
    this.$contentEle.focus();
  }

  addMarker(marker: Marker) {
    this.markers[marker.id] = marker;
  }

  removeMarker(marker: Marker) {
    delete this.markers[marker.id];
  }

  triggerSpellCheckWorker() {
    // Hack for Chrome to trigger the spell checker
    const selection = window.getSelection()!;
    if (this.selectionMgr.hasFocus()
      && !this.highlighter.isComposing
      && this.selectionMgr.selectionStart === this.selectionMgr.selectionEnd
      && selection.modify
    ) {
      if (this.selectionMgr.selectionStart) {
        selection.modify('move', 'backward', 'character');
        selection.modify('move', 'forward', 'character');
      } else {
        selection.modify('move', 'forward', 'character');
        selection.modify('move', 'backward', 'character');
      }
    }
  }

  checkContentChange(mutations: MutationRecord[]) {
    this.watcher.noWatch(() => {
      const removedSections: Section[] = [];
      const modifiedSections: Section[] = [];

      const markModifiedSection = (node: HTMLElement) => {
        let currentNode = node;
        while (currentNode && currentNode !== this.$contentEle) {
          if (currentNode.section) {
            const array = currentNode.parentNode ? modifiedSections : removedSections;
            if (array.indexOf(currentNode.section) === -1) {
              array.push(currentNode.section);
            }
            return;
          }
          currentNode = currentNode.parentNode! as HTMLElement;
        }
      };

      mutations.forEach((mutation: MutationRecord) => {
        markModifiedSection(mutation.target as HTMLElement);
        Array.prototype.slice.call(mutation.addedNodes, 0)
          .forEach(markModifiedSection);
        Array.prototype.slice.call(mutation.removedNodes, 0)
          .forEach(markModifiedSection);
      });
      this.highlighter.fixContent(modifiedSections, removedSections, this.noContentFix);
      this.noContentFix = false;
    });

    if (!this.skipSaveSelection) {
      this.selectionMgr.saveSelectionState();
    }

    this.skipSaveSelection = false;

    const newTextContent = this.getTextContent();
    const diffs = diffMatchPatch.diff_main(this.lastTextContent, newTextContent);
    this.markers.forEach(marker => {
      marker.adjustOffset(diffs);
    });

    const sectionList = this.highlighter.parseSections(newTextContent);
    this.$events.$emit(CORE_CONTENT_CHANGED, newTextContent, diffs, sectionList);
    if (!this.ignoreUndo) {
      this.undoMgr.addDiffs(this.lastTextContent, newTextContent, diffs);
      this.undoMgr.setDefaultMode('typing');
      this.undoMgr.saveState();
    }

    this.ignoreUndo = false;
    this.lastTextContent = newTextContent;
    this.triggerSpellCheck();
  }

  setSelection(start: number, end: number) {
    this.selectionMgr.setSelectionStartEnd(start, end == null ? start : end);
    this.selectionMgr.updateCursorCoordinates();
  }

  justInputEvent(handle: Function) {
    // Just handle the input event,
    return (event: KeyboardEvent) => {
      // Ctrl、Cmd、Alt、Shift
      if ([17, 91, 18, 16].indexOf(event.which) === -1) {
        handle.call(this, event);
      }
    };
  }

  // In case of Ctrl/Cmd+A outside the editor element
  windowOnKeydown(event: KeyboardEvent) {
    if (!this.tryDestroy()) {
      this.justInputEvent(() => {
        this.adjustCursorPosition();
      })(event);
    }
  }

  // Mouseup can happen outside the editor element
  windowOnMouseEvent() {
    if (!this.tryDestroy()) {
      this.selectionMgr.saveSelectionState(true, false);
    }
  }

  // Resize provokes cursor coordinate changes
  windowOnResize() {
    if (!this.tryDestroy()) {
      this.selectionMgr.updateCursorCoordinates();
    }
  }

  listenWindowEvent() {
    window.addEventListener('keydown', this.windowOnKeydown.bind(this));
    window.addEventListener('mousedown', this.windowOnMouseEvent.bind(this));
    window.addEventListener('mouseup', this.windowOnMouseEvent.bind(this));
    window.addEventListener('resize', this.windowOnResize.bind(this));
  }

  tryDestroy() {
    if (document.contains(this.$contentEle)) {
      return false;
    }

    this.watcher.stopWatching();

    window.removeEventListener('keydown', this.windowOnKeydown.bind(this));
    window.removeEventListener('mousedown', this.windowOnMouseEvent.bind(this));
    window.removeEventListener('mouseup', this.windowOnMouseEvent.bind(this));
    window.removeEventListener('resize', this.windowOnResize.bind(this));

    this.$events.$emit(CORE_DESTROY);
    return true;
  }

  contentEleOnInput(event: KeyboardEvent) {
    this.selectionMgr.saveSelectionState();

    // Perform keystroke
    let contentChanging = false;
    const textContent = this.getTextContent();
    let min = Math.min(this.selectionMgr.selectionStart, this.selectionMgr.selectionEnd);
    let max = Math.max(this.selectionMgr.selectionStart, this.selectionMgr.selectionEnd);
    const state: SelectionState = {
      before: textContent.slice(0, min),
      after: textContent.slice(max),
      selection: textContent.slice(min, max),
      isBackwardSelection: this.selectionMgr.selectionStart > this.selectionMgr.selectionEnd,
    };

    ArrHasBy(this.keystrokes, (keystroke: Keystroke) => {
      if (!keystroke.handler(event, state, this)) {
        return false;
      }
      const newContent = state.before + state.selection + state.after;
      if (newContent !== this.getTextContent()) {
        this.setContent(newContent, false, min);
        contentChanging = true;
        this.skipSaveSelection = true;
        this.highlighter.cancelComposition = true;
      }
      min = state.before.length;
      max = min + state.selection.length;
      this.selectionMgr.setSelectionStartEnd(
        state.isBackwardSelection ? max : min,
        state.isBackwardSelection ? min : max,
        !contentChanging, // Expect a restore selection on mutation event
      );
      return true;
    });

    if (!contentChanging) {
      // Optimization to avoid saving selection
      this.adjustCursorPosition();
    }
  }

  contentEleOnCompositionEnd() {
    setTimeout(() => {
      if (this.highlighter.isComposing) {
        this.highlighter.isComposing -= 1;
        if (!this.highlighter.isComposing) {
          this.checkContentChange([]);
        }
      }
    }, 1);
  }

  contentEleOnCopy(event: ClipboardEvent) {
    if (event.clipboardData) {
      event.clipboardData.setData('text/plain', this.selectionMgr.getSelectedText());
      event.preventDefault();
    }
  }

  contentEleOnCut(event: ClipboardEvent) {
    if (event.clipboardData) {
      event.clipboardData.setData('text/plain', this.selectionMgr.getSelectedText());
      event.preventDefault();
      this.replaceAt(this.selectionMgr.selectionStart, this.selectionMgr.selectionEnd, '');
    } else {
      this.undoMgr.setCurrentMode('single');
    }
    this.adjustCursorPosition();
  }

  contentEleOnPaste(event: ClipboardEvent) {
    this.undoMgr.setCurrentMode('single');
    event.preventDefault();

    let data;
    let { clipboardData } = event;

    if (clipboardData) {
      data = clipboardData.getData('text/plain');
      if (this.isMarkdown && clipboardData.types.indexOf('text/html') > -1) {
        const html = clipboardData.getData('text/html');
        if (html) {
          const sanitizedHtml = sanitizeHtml(html, this.settings.sanitizeOptions)
            .replace(/&#160;/g, ' '); // Replace non-breaking spaces with classic spaces
          if (sanitizedHtml) {
            data = this.turndownService.turndown(sanitizedHtml);
          }
        }
      }
    } else {
      ({ clipboardData } = window);
      data = clipboardData && clipboardData.getData('Text');
    }
    if (!data) {
      return;
    }
    this.replaceAt(this.selectionMgr.selectionStart, this.selectionMgr.selectionEnd, data);
    this.adjustCursorPosition();
  }

  listenContentEleEvent() {
    // Provokes selection changes and does not fire mouseup event on Chrome/OSX
    this.$contentEle.addEventListener('contextmenu', functionBindWith(this.selectionMgr.saveSelectionState, this.selectionMgr, true, false));
    this.$contentEle.addEventListener('keydown', this.justInputEvent(this.contentEleOnInput));
    this.$contentEle.addEventListener('compositionstart', () => this.highlighter.isComposing++);
    this.$contentEle.addEventListener('compositionend', this.contentEleOnCompositionEnd.bind(this));

    this.$contentEle.addEventListener('focus', () => this.$events.$emit(CORE_FOCUS));
    this.$contentEle.addEventListener('blur', () => this.$events.$emit(CORE_BLUR));
    this.$contentEle.addEventListener('paste', this.contentEleOnPaste.bind(this));

    if (this.isMarkdown) {
      this.$contentEle.addEventListener('copy', this.contentEleOnCopy.bind(this));
      this.$contentEle.addEventListener('cut', this.contentEleOnCut.bind(this));
    }
  }

  addKeystroke(keystroke: Keystroke | Keystroke[]) {
    const keystrokes: Keystroke[] = Array.isArray(keystroke) ? keystroke : [keystroke];
    this.keystrokes = this.keystrokes
      .concat(keystrokes)
      .sort((keystroke1, keystroke2) => keystroke1.priority - keystroke2.priority);
  }
}
