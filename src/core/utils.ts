
export const isGecko = 'MozAppearance' in document.documentElement.style;
export const isWebkit = 'WebkitAppearance' in document.documentElement.style;
export const isMsie = 'msTransform' in document.documentElement.style;
export const isMac = navigator.userAgent.indexOf('Mac OS X') !== -1;

// Faster than setTimeout(0). Credit: https://github.com/stefanpenner/es6-promise
export const defer = (() => {
  const queue = new Array(1000);
  let queueLength = 0;
  function flush() {
    for (let i = 0; i < queueLength; i += 1) {
      try {
        queue[i]();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e.message, e.stack);
      }
      queue[i] = undefined;
    }
    queueLength = 0;
  }

  let iterations = 0;
  const observer = new MutationObserver(flush);
  const node = document.createTextNode('');
  observer.observe(node, { characterData: true });

  return (func: Function) => {
    queue[queueLength] = func;
    queueLength += 1;
    if (queueLength === 1) {
      iterations = (iterations + 1) % 2;
      node.data = `${iterations}`;
    }
  };
})();

export function debounce(func: Function, wait: number = 0, context: any = null, ...args: any[]) {
  let timeoutId: number;
  let isExpected: boolean;
  return wait
    ? () => {
      clearTimeout(timeoutId);
      timeoutId = window.setTimeout(() => func.apply(context, args), wait);
    }
    : () => {
      if (!isExpected) {
        isExpected = true;
        defer(() => {
          isExpected = false;
          func.apply(context, args);
        });
      }
    };
}

export function findContainer(ele: HTMLElement, offset: number) {
  let containerOffset = 0;
  let container;
  let child = ele;
  do {
    container = child;
    child = child.firstChild! as HTMLElement;
    if (child) {
      do {
        const len = child.textContent!.length;
        if (containerOffset <= offset && containerOffset + len > offset) {
          break;
        }
        containerOffset += len;
        child = child.nextSibling! as HTMLElement;
      } while (child);
    }
  } while (child && child.firstChild && child.nodeType !== 3);

  if (child) {
    return {
      container: child,
      offsetInContainer: offset - containerOffset,
    };
  }

  while (container.lastChild) {
    container = container.lastChild as HTMLElement;
  }

  return {
    container,
    offsetInContainer: container.nodeType === 3 ? container.textContent!.length : 0,
  };
}
