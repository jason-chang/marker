
import { Editor } from '@/libs/editor/editor';
import Vue from 'vue';

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    events?: Vue;
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    editor: Editor,
    $editor: Editor,
    $events: Vue;
  }
}
