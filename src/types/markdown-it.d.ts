
declare module 'markdown-it' {
  import Mdd, { Options, Rule as Ru } from 'markdown-it/lib';

  module MarkdownIt {
    interface Rule extends Ru {}
  }

  class MarkdownIt extends Mdd {
    options: Options;
    use(plugin: (md: MarkdownIt, ...params: any[]) => void, ...params: any[]): MarkdownIt;
  }

  export = MarkdownIt;
}
