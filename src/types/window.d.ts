import { VueConstructor } from 'vue';
import Clipboard from 'clipboard';
import { Section } from '@/core/section';
import { Animation } from '@/libs/animation';

declare global {
  interface HTMLElement {
    clipboard?: Clipboard,
    section?: Section,
    highlighted?: boolean,
    $animation?: Animation,
    $highlightedWithPrism?: boolean,
  }

  interface Selection {
    modify: (alter: string, direction: string, granularity: string) => void,
  }

  interface Window {
    clipboardData: DataTransfer,
    Vue?: VueConstructor,
  }
}
