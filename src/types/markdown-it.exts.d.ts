
declare module 'markdown-it-abbr' {
  import MarkdownIt = require('markdown-it');

  const markdownItAbbr: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItAbbr;
}

declare module 'markdown-it-deflist' {
  import MarkdownIt = require('markdown-it');

  const markdownItDeflist: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItDeflist;
}

declare module 'markdown-it-emoji' {
  import MarkdownIt = require('markdown-it');

  const markdownItEmoji: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItEmoji;
}

declare module 'markdown-it-footnote' {
  import MarkdownIt = require('markdown-it');

  const markdownItFootnote: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItFootnote;
}

declare module 'markdown-it-imsize' {
  import MarkdownIt = require('markdown-it');

  const markdownItImsize: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItImsize;
}

declare module 'markdown-it-mark' {
  import MarkdownIt = require('markdown-it');

  const markdownItMark: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItMark;
}

declare module 'markdown-it-sub' {
  import MarkdownIt = require('markdown-it');

  const markdownItSub: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItSub;
}

declare module 'markdown-it-sup' {
  import MarkdownIt = require('markdown-it');

  const markdownItSup: (md: MarkdownIt, ...params: any[]) => void;
  export default markdownItSup;
}
