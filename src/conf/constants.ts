const origin = `${window.location.protocol}//${window.location.host}`;

export default {
  cleanTrashAfter: 7 * 24 * 60 * 60 * 1000, // 7 days
  origin,
  oauth2RedirectUri: `${origin}/oauth2/callback`,
  types: [
    'contentState',
    'content',
    'file',
    'folder',
    'data',
  ],
  textMaxLength: 250000,
  defaultName: 'Untitled',
};
