import { PagedownButtonConfig } from '@/libs/pagedown/pagedown';

const pagedownButtons: PagedownButtonConfig[] = [
  {},
  { method: 'bold', title: 'Bold', icon: 'toolbar-bold' },
  { method: 'italic', title: 'Italic', icon: 'toolbar-italic' },
  { method: 'heading', title: 'Heading', icon: 'toolbar-heading' },
  { method: 'strikethrough', title: 'Strikethrough', icon: 'toolbar-strike-through' },
  {},
  { method: 'ulist', title: 'Unordered list', icon: 'toolbar-list-unordered' },
  { method: 'olist', title: 'Ordered list', icon: 'toolbar-list-ordered' },
  { method: 'clist', title: 'Check list', icon: 'toolbar-list-check' },
  {},
  { method: 'quote', title: 'Blockquote', icon: 'toolbar-blockquote' },
  { method: 'code', title: 'Code', icon: 'toolbar-code' },
  { method: 'table', title: 'Table', icon: 'toolbar-table' },
  { method: 'link', title: 'Link', icon: 'toolbar-link' },
  { method: 'image', title: 'Image', icon: 'toolbar-image' },
];

export default pagedownButtons;
