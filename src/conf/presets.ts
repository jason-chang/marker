import merge from 'lodash/merge';

const zero = {
  // Markdown extensions
  markdown: {
    abbr: false,
    breaks: false,
    deflist: false,
    del: false,
    fence: false,
    footnote: false,
    imgsize: false,
    linkify: false,
    mark: false,
    sub: false,
    sup: false,
    table: false,
    tasklist: false,
    typographer: false,
  },
  // Emoji extension
  emoji: {
    enabled: false,
    // Enable shortcuts like :) :-(
    shortcuts: false,
  },
  /*
  Katex extension
  Render LaTeX mathematical expressions using:
    $...$ for inline formulas
    $$...$$ for displayed formulas.
  See https://math.meta.stackexchange.com/questions/5020
  */
  katex: {
    enabled: false,
  },
};

const presets = {
  zero,
  commonmark: merge(zero, {
    markdown: {
      fence: true,
    },
  }),
  gfm: merge(zero, {
    markdown: {
      breaks: true,
      del: true,
      fence: true,
      linkify: true,
      table: true,
      tasklist: true,
    },
    emoji: {
      enabled: true,
    },
  }),
  default: merge(zero, {
    markdown: {
      abbr: true,
      breaks: true,
      deflist: true,
      del: true,
      fence: true,
      footnote: true,
      imgsize: true,
      linkify: true,
      mark: true,
      sub: true,
      sup: true,
      table: true,
      tasklist: true,
      typographer: true,
    },
    emoji: {
      enabled: true,
    },
    katex: {
      enabled: true,
    },
  }),
};

export default presets;
