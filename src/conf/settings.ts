import { Options } from 'turndown';
import sanitizeHtml from 'sanitize-html';
import merge from 'lodash/merge';

export const sanitizeOptions: sanitizeHtml.IOptions = merge(sanitizeHtml.defaults, {
  allowedTags: ['blockquote', 'code', 'em', 'div', 'h1', 'h2', 'line', 'g', 'p', 'rect', 'span', 'strong', 'style', 'svg', 'text', 'tspan'],
  allowedAttributes: {
    div: ['class'],
    span: ['class'],
    code: ['class'],
  },
});

export const turndown: Options = {
  headingStyle: 'atx',
  hr: '----------',
  bulletListMarker: '-',
  codeBlockStyle: 'fenced',
  fence: '```',
  emDelimiter: '_',
  strongDelimiter: '**',
  linkStyle: 'inlined',
  linkReferenceStyle: 'full',
};

export type ExpandItem = {
  method: string,
  params: [string, string],
};

export type EditActions = 'find' | 'replace' | 'bold' | 'clist' | 'code' | 'heading' | 'hr' | 'image' | 'italic' | 'link' | 'olist' | 'quote' | 'strikethrough' | 'table' | 'ulist';

export type Shortcuts = { [key: string]: EditActions };

const shortcuts: Shortcuts = {
  'mod+f': 'find',
  'mod+alt+f': 'replace',
  'mod+g': 'replace',
  'mod+shift+b': 'bold',
  'mod+shift+c': 'clist',
  'mod+shift+k': 'code',
  'mod+shift+h': 'heading',
  'mod+shift+r': 'hr',
  'mod+shift+g': 'image',
  'mod+shift+i': 'italic',
  'mod+shift+l': 'link',
  'mod+shift+o': 'olist',
  'mod+shift+q': 'quote',
  'mod+shift+s': 'strikethrough',
  'mod+shift+t': 'table',
  'mod+shift+u': 'ulist',
};

export const expands: { [key: string]: ExpandItem } = {
  '= = > space': {
    method: 'expand',
    params: [
      '==> ',
      '⇒ ',
    ],
  },
  '< = = space': {
    method: 'expand',
    params: [
      '<== ',
      '⇐ ',
    ],
  },
};

export type Settings = {
  sanitizeOptions?: sanitizeHtml.IOptions,
  shortcuts?: Shortcuts,
  expands?: { [key: string]: ExpandItem },
  turndown?: Options,
  theme?: string,
  editor?: {
    inlineImages?: boolean,
    monospacedFontOnly?: boolean,
  },
}

export const defaultSetting: Settings = {
  sanitizeOptions,
  shortcuts,
  expands,
  turndown,
  theme: 'light',
  editor: {
    inlineImages: true,
    monospacedFontOnly: false,
  },
};

let userSettings: Settings = defaultSetting;

export function changeSettings(settings = {}) {
  userSettings = merge(defaultSetting, settings);
}

export function getSettings(toMerge = {}) {
  return merge(userSettings, toMerge);
}
