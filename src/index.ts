
import { VueConstructor } from 'vue';
import { changeSettings } from '@/conf/settings';
import component from './main';

function install(Vue: VueConstructor) {
  Vue.component(process.env.VUE_APP_COMPONENT_ID!, component);
}

const conf = {
  changeSettings,
  install,
  component,
};

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(conf);
}

if (typeof window !== 'undefined') {
  // @ts-ignore
  window[process.env.VUE_APP_COMPONENT_ID] = conf;
}

export default conf;
