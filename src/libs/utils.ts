
const uidLength = 16;
const alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
const radix = alphabet.length;
const array = new Uint32Array(uidLength);

// For utils.parseQueryParams()
export function parseQueryParams(params: string) {
  const result: { [prop: string]: string } = {};
  params.split('&').forEach(param => {
    const [key, value] = param.split('=').map(decodeURIComponent);
    if (key && value != null) {
      result[key] = value;
    }
  });
  return result;
}

// For utils.setQueryParams()
function filterParams(params = {}) {
  const result: { [prop: string]: any } = {};
  Object.entries(params).forEach(([key, value]) => {
    if (key && value != null) {
      result[key] = value;
    }
  });
  return result;
}

// For utils.addQueryParams()
const urlParser = document.createElement('a');

export function deepCopy(obj: {}) {
  if (obj == null) {
    return obj;
  }
  return JSON.parse(JSON.stringify(obj));
}

export function serializeObject(obj: any) {
  return obj === undefined ? obj : JSON.stringify(obj, (key, value) => {
    if (Object.prototype.toString.call(value) !== '[object Object]') {
      return value;
    }
    // Sort keys to have a predictable result
    return Object.keys(value).sort().reduce((
      sorted: { [key: string]: string },
      valueKey,
    ) => {
      sorted[valueKey] = value[valueKey];
      return sorted;
    }, {});
  });
}

export function uid() {
  crypto.getRandomValues(array);
  return Array.prototype.slice.call(array).map(value => alphabet[value % radix]).join('');
}

export function hash(str: string) {
  // https://stackoverflow.com/a/7616484/1333165
  let hashStr = 0;
  if (!str) return hashStr;
  for (let i = 0; i < str.length; i++) {
    const char = str.charCodeAt(i);
    hashStr = ((hashStr << 5) - hashStr) + char; // eslint-disable-line no-bitwise
    hashStr |= 0; // eslint-disable-line no-bitwise
  }
  return hashStr;
}

export function randomize(value: number) {
  return Math.floor((1 + (Math.random() * 0.2)) * value);
}

export function setRandomInterval(func: Function, interval: number) {
  return setInterval(() => func(), randomize(interval));
}

export async function awaitSequence(values: Array<any>, asyncFunc: (item: any) => Promise<any>) {
  const results: Array<any> = [];
  const valuesLeft = values.slice().reverse();
  const runWithNextValue = async (): Promise<Promise<any> | Array<any>> => {
    if (!valuesLeft.length) {
      return results;
    }
    results.push(await asyncFunc(valuesLeft.pop()));
    return runWithNextValue();
  };
  return runWithNextValue();
}

export function wrapRange(range: Range, eleProperties: object) {
  const rangeLength = `${range}`.length;
  let wrappedLength = 0;
  const treeWalker = document.createTreeWalker(range.commonAncestorContainer, NodeFilter.SHOW_TEXT);
  let { startOffset } = range;
  treeWalker.currentNode = range.startContainer;

  if (treeWalker.currentNode.nodeType !== Node.TEXT_NODE && !treeWalker.nextNode()) {
    return;
  }

  do {
    if (treeWalker.currentNode.nodeValue !== '\n') {
      if (treeWalker.currentNode === range.endContainer
        && range.endOffset < treeWalker.currentNode.nodeValue!.length
      ) {
        (treeWalker.currentNode! as Text).splitText(range.endOffset);
      }
      if (startOffset) {
        treeWalker.currentNode = (treeWalker.currentNode as Text).splitText(startOffset);
        startOffset = 0;
      }
      const ele = document.createElement('span');
      Object.entries(eleProperties).forEach(([key, value]) => {
        ele.setAttribute(key, value);
      });
      treeWalker.currentNode.parentNode!.insertBefore(ele, treeWalker.currentNode);
      ele.appendChild(treeWalker.currentNode);
    }

    wrappedLength += treeWalker.currentNode.nodeValue!.length;
    if (wrappedLength >= rangeLength) {
      break;
    }
  } while (treeWalker.nextNode());
}

export function unwrapRange(eleCollection: HTMLCollectionOf<Element>) {
  Array.prototype.slice.call(eleCollection).forEach(elt => {
    // Loop in case another wrapper has been added inside
    for (let child = elt.firstChild; child; child = elt.firstChild) {
      if (child.nodeType === 3) {
        if (elt.previousSibling && elt.previousSibling.nodeType === 3) {
          child.nodeValue = elt.previousSibling.nodeValue + child.nodeValue;
          elt.parentNode.removeChild(elt.previousSibling);
        }
        if (!child.nextSibling && elt.nextSibling && elt.nextSibling.nodeType === 3) {
          child.nodeValue += elt.nextSibling.nodeValue;
          elt.parentNode.removeChild(elt.nextSibling);
        }
      }
      elt.parentNode.insertBefore(child, elt);
    }
    elt.parentNode.removeChild(elt);
  });
}

/* =--------------------------= sanitizer =--------------------------= */
const aHrefSanitizationWhitelist = /^\s*(https?|ftp|mailto|tel|file):/;
const imgSrcSanitizationWhitelist = /^\s*((https?|ftp|file|blob):|data:image\/)/;
const urlParsingNode = window.document.createElement('a');

export function sanitizeUri(uri: string, isImage: boolean) {
  const regex = isImage ? imgSrcSanitizationWhitelist : aHrefSanitizationWhitelist;
  urlParsingNode.setAttribute('href', uri);
  const normalizedVal = urlParsingNode.href;
  if (normalizedVal !== '' && !normalizedVal.match(regex)) {
    return `unsafe:${normalizedVal}`;
  }
  return uri;
}

export function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}

export function allowDebounce(action: Function, wait: number) {
  let timeoutId: number;
  return (doDebounce = false, ...params: any[]) => {
    clearTimeout(timeoutId);
    if (doDebounce) {
      timeoutId = window.setTimeout(() => action(...params), wait);
    } else {
      action(...params);
    }
  };
}
