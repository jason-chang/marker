import bezierEasing from 'bezier-easing';

export const easings: { [key: string]: BezierEasing.EasingFunction } = {
  'ease-in': bezierEasing(0.75, 0, 0.8, 0.25),
  'ease-out': bezierEasing(0.25, 0.8, 0.25, 1),
  'ease-in-out': bezierEasing(0.25, 0.1, 0.67, 1),
};

const transformStyles = [
  'WebkitTransform',
  'MozTransform',
  'msTransform',
  'OTransform',
  'transform',
];

const transitionEndEvents: { [key: string]: string } = {
  WebkitTransition: 'webkitTransitionEnd',
  MozTransition: 'transitionend',
  msTransition: 'MSTransitionEnd',
  OTransition: 'oTransitionEnd',
  transition: 'transitionend',
};

function getAvailableStyle(styles: string[]) {
  const ele = document.createElement('div');
  return styles.reduce((result: any, style: string) => {
    if (ele.style[style as any] === undefined) {
      return undefined;
    }
    return style;
  }, undefined);
}

const transformStyle = getAvailableStyle(transformStyles);
const transitionStyle = getAvailableStyle(Object.keys(transitionEndEvents));

function identity(x: any) {
  return x;
}

interface AnimationAttributes {
  name: string,
  setStart: (animation: Animation) => any;
}

type AttributeToSet = 'scrollTop' | 'scrollLeft';

class ElementAttribute implements AnimationAttributes {
  constructor(
    public name:string,
  ) {
    // not empty
  }

  setStart(animation: Animation) {
    const value = animation.ele[this.name as AttributeToSet];
    animation.$start[this.name] = value;
    return value !== undefined && animation.$end[this.name] !== undefined;
  }

  applyCurrent(animation: Animation) {
    animation.ele[this.name as AttributeToSet] = animation.$current[this.name];
  }
}

class StyleAttribute implements AnimationAttributes {
  constructor(
    public name: string,
    private unit: string,
    private defaultValue: number,
    private wrap = identity,
  ) {
    // not empty
  }

  setStart(animation: Animation) {
    let value = parseFloat(animation.ele.style.getPropertyValue(this.name));

    if (Number.isNaN(value)) {
      value = animation.$current[this.name] || this.defaultValue;
    }

    animation.$start[this.name] = value;
    return animation.$end[this.name] !== undefined;
  }

  applyCurrent(animation: Animation) {
    animation.ele.style.setProperty(this.name, this.wrap(animation.$current[this.name]) + this.unit);
  }
}

class TransformAttribute implements AnimationAttributes {
  constructor(
    public name: string,
    private unit: string,
    private defaultValue: number,
    private wrap = identity,
  ) {
    this.name = name;
  }

  setStart(animation: Animation) {
    let value = animation.$current[this.name];

    if (value === undefined) {
      value = this.defaultValue;
    }

    animation.$start[this.name] = value;

    if (animation.$end[this.name] === undefined) {
      animation.$end[this.name] = value;
    }

    return value !== undefined;
  }

  applyCurrent(animation: Animation) {
    const value = animation.$current[this.name];
    return value !== this.defaultValue && `${this.name}(${this.wrap(value)}${this.unit})`;
  }
}

const attributes: AnimationAttributes[] = [
  new ElementAttribute('scrollTop'),
  new ElementAttribute('scrollLeft'),
  new StyleAttribute('opacity', '', 1),
  new StyleAttribute('zIndex', '', 0),
  new TransformAttribute('translateX', 'px', 0, Math.round),
  new TransformAttribute('translateY', 'px', 0, Math.round),
  new TransformAttribute('scale', '', 1),
  new TransformAttribute('rotate', 'deg', 0),
].concat(
  ['width', 'height', 'top', 'right', 'bottom', 'left'].map(name => new StyleAttribute(name, 'px', 0, Math.round)),
);

interface AnimationEndOption {
  duration?: number,
  delay?: number,
  easing: string,
  endCb?: Function,
  stepCb?: Function,
  [key: string]: any,
}

export class Animation {

  $start: { [key: string]: any } = {};
  $end: AnimationEndOption = {
    duration: 0,
    delay: 0,
    easing: 'ease-in',
    endCb() {},
    stepCb() {},
  };

  $pending: AnimationEndOption = {
    duration: 0,
    easing: 'ease-in-out',
  };

  $startTime: number = 0;
  $attributes!: any[];
  $current: {[key: string]: number } = {};
  $requestId!: number;

  duration!: animationSetter;
  easing!: animationSetter;
  delay!: animationSetter;
  scrollTop!: animationSetter;

  [key: string]: any;

  constructor(
    public ele: HTMLElement,
  ) {
    // not empty
  }

  start(
    endCb: Function = () => {},
    stepCb: Function = () => {},
  ) {
    this.stop();
    this.$start = {};
    this.$end = this.$pending;

    this.$attributes = attributes.filter(attribute => attribute.setStart(this));

    this.$end.duration = this.$end.duration || 0;
    this.$end.delay = this.$end.delay || 0;
    this.$end.endCb = endCb;
    this.$end.stepCb = stepCb;

    this.$startTime = Date.now() + this.$end.delay;

    if (!this.$end.duration) {
      this.loop();
    } else {
      this.$requestId = window.requestAnimationFrame(() => this.loop());
    }
    return this.ele;
  }

  stop() {
    window.cancelAnimationFrame(this.$requestId);
  }

  loop() {
    const progress = (Date.now() - this.$startTime) / this.$end.duration!;
    const transition = '';
    if (progress < 1) {
      this.$requestId = window.requestAnimationFrame(() => this.loop());
      if (progress < 0) {
        return;
      }
    } else if (this.$end.endCb) {
      this.$requestId = window.requestAnimationFrame(this.$end.endCb as FrameRequestCallback);
    }

    const coeff = easings[this.$end.easing](progress);

    const transforms = this.$attributes.reduce((result, attribute) => {
      if (progress < 1) {
        const diff = this.$end[attribute.name] - this.$start[attribute.name];
        this.$current[attribute.name] = this.$start[attribute.name] + (diff * coeff);
      } else {
        this.$current[attribute.name] = this.$end[attribute.name];
      }

      const transform = attribute.applyCurrent(this);
      if (transform) {
        result.push(transform);
      }
      return result;
    }, []);

    if (transforms.length) {
      transforms.push('translateZ(0)'); // activate GPU
    }
    const transform = transforms.join(' ');
    this.ele.style[transformStyle! as any] = transform;
    this.ele.style[transitionStyle! as any] = transition;
    this.$end.stepCb!();
  }
}

export type animationSetter = (val: any) => Animation;

function createAnimationSetter(name: string): animationSetter {
  // eslint-disable-next-line func-names
  return function (this: Animation, val: any) {
    this.$pending[name] = val;
    return this;
  };
}

attributes
  .map(attribute => attribute.name)
  .concat('duration', 'easing', 'delay')
  .forEach(name => {
    Animation.prototype[name] = createAnimationSetter(name);
  });

export function animate(ele: HTMLElement): Animation {
  if (!ele.$animation) {
    ele.$animation = new Animation(ele);
  }
  return ele.$animation;
}
