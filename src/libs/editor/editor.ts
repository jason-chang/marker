/* eslint-disable camelcase */
import Vue from 'vue';
import MarkdownIt from 'markdown-it';
import Token from 'markdown-it/lib/token';
import DiffMatchPatch, { Diff } from 'diff-match-patch';
import Prism from 'prismjs';
import sanitizeHtml from 'sanitize-html';

import { Editor as CoreEditor } from '@/core';
import { allowDebounce, applyMixins, sanitizeUri, serializeObject } from '@/libs/utils';
import { Section } from '@/core/section';
import { EditorUtils } from '@/libs/editor/editorUtils';
import { EditorOptions } from '@/core/editor';
import { Pagedown } from '@/libs/pagedown/pagedown';
import { convert, createConverter, parseSections, defaultOptions, defaultProperties, ParseingCtx } from '@/libs/conversion';
import { makeGrammars } from '@/libs/grammar';
import { measureSectionDimensions, SectionDimension } from '@/libs/editor/sectionUtils';
import { CORE_BLUR, CORE_CONTENT_CHANGED, CORE_FOCUS, CORE_HIGHLIGHTED, CORE_SECTION_HIGHLIGHTED, CORE_SELECTION_CHANGED, CORE_UNDO_STATE_CHANGE } from '@/core/events';
import { debounce } from '@/core/utils';
import { precessOptions, renderSection } from '@/libs/converter';

import { activeScrollSync } from '@/libs/optional/scrollSync';
import { create } from '@/core/create';
import { additionalKeystrokes } from '@/libs/editor/keystrokes';
import { ShortcutManager } from '@/libs/optional/shortcuts';
import { Settings } from '@/conf/settings';
import pagedown from '@/libs/pagedown';
import Main from '@/components/Main.vue';

export const MEE_INITED = 'MEE_INITED';
export const MEE_PREVIEW_CTX_MEASURED = 'PREVIEW_CTX_MEASURED';
export const MEE_PREVIEW_CTX_WITH_DIFFS = 'PREVIEW_CTX_WITH_DIFFS';
export const MEE_SECTION_LIST = 'MEE_SECTION_LIST';
export const MEE_PREVIEW_CTX = 'MEE_PREVIEW_CTX';
export const MEE_SELECTION_RANGE = 'MEE_SELECTION_RANGE';
export const MEE_CONVERSION_CTX = 'MEE_CONVERSION_CTX';
export const MEE_PREVIEW_SELECTION_RANGE = 'MEE_PREVIEW_SELECTION_RANGE';

export interface EditorCtx {
  sections?: [],
  sectionList?: Section[],
  markdown?: string,
  html?: string,
  text?: string,
  sectionInfoList?: SectionInfo[],
  htmlSectionList?: string[],
  htmlSectionDiff?: Diff[],
}

// eslint-disable-next-line new-cap
const diffMatchPatch = new DiffMatchPatch.diff_match_patch();
let instantPreview = true;
let tokens: Token[];


export type DimensionNames = 'editorDimension' | 'previewDimension' | 'tocDimension';

export class SectionInfo {
  editorEle: HTMLElement;

  startOffset!: number;
  endOffset!: number;
  height!: number;

  editorDimension!: SectionDimension;
  previewDimension!: SectionDimension;
  tocDimension!: SectionDimension;

  textToPreviewDiffs!: Diff[];

  previewText!: string;

  // [key: string]: SectionInfo | any;

  constructor(
    public section: Section,
    public previewEle: HTMLElement,
    public tocEle: HTMLElement,
    public html: string,
  ) {
    this.editorEle = section.ele;
    this.html = html;
  }
}

export class Editor implements EditorUtils {

  inined = false;

  editorEle!: HTMLElement;
  previewEle!: HTMLElement;
  tocEle!: HTMLElement;

  core!: CoreEditor;

  // Other objects
  pagedownEditor!: Pagedown;
  options!: {};
  prismGrammars!: { [key: string]: any };
  converter!: MarkdownIt;
  parsingCtx!: ParseingCtx;
  conversionCtx!: ParseingCtx;
  previewCtx!: EditorCtx;
  previewCtxMeasured!: EditorCtx | null;
  previewCtxWithDiffs!: EditorCtx | null;
  sectionList!: Section[];
  selectionRange!: Range;
  previewSelectionRange?: Range;

  syncSelectionActived = false;

  // EditorSvcUtils
  getScrollPosition!: (ele?: HTMLElement) => any;
  getEditorOffset!: (previewOffset: number, sectionInfoList?: SectionInfo[] | undefined) => null | number;

  measureSectionDimensions: Function;
  syncSelection: Function;
  shortcutManager: ShortcutManager;

  get $store() {
    return this.$marker!.$store;
  }

  /**
   *
   * @param $marker
   * @param $events
   * @param settings
   */
  constructor(
    public $marker: Main,
    public $events: Vue,
    public settings: Settings,
  ) {
    this.measureSectionDimensions = allowDebounce(this.measureSectionDimensionsWorker.bind(this), 500);
    this.syncSelection = allowDebounce(this.syncSelectionWorker.bind(this), 50);
    this.shortcutManager = new ShortcutManager(this);
  }

  doWhenInited(cb: Function) {
    if (this.inined) {
      cb();
    } else {
      this.$events.$once(MEE_INITED, cb);
    }
  }

  /**
   * Pass the elements to the store and initialize the editor.
   */
  init(
    editorEle: HTMLElement,
    previewEle: HTMLElement,
    tocEle: HTMLElement,
  ) {
    this.editorEle = editorEle;
    this.previewEle = previewEle;
    this.tocEle = tocEle;

    this.createCore();

    this.initPagedownEditor();

    this.initCoreImageCache();

    this.activeSyncTaskList();

    this.activePreviewRefresher();

    additionalKeystrokes(this);
    activeScrollSync(this);

    this.$events.$emit(MEE_INITED, this);
  }

  createCore() {
    this.core = create(
      this.$events,
      this.editorEle,
      this.editorEle.parentNode as HTMLElement,
      this.settings,
      true,
    );

    this.core.$events.$on(CORE_CONTENT_CHANGED, (content: string, diffs: Diff[], sectionList: Section[]) => {
      this.parsingCtx = {
        ...this.parsingCtx,
        sectionList,
      };
    });

    this.core.$events.$on(CORE_UNDO_STATE_CHANGE, () => {
      this.$store.commit('setState', {
        canUndo: this.core.undoMgr.canUndo(),
        canRedo: this.core.undoMgr.canRedo(),
      });
    });

    this.core.$events.$on(CORE_FOCUS, () => {
      this.shortcutManager.active();
      this.activeSyncSelection();
    });

    this.core.$events.$on(CORE_BLUR, () => {
      this.shortcutManager.deactive();
      this.deactiveSyncSelection();
    });
  }

  /**
   * Initialize pagedown editor
   */
  initPagedownEditor() {
    this.pagedownEditor = pagedown({ input: Object.create(this.core) });
    this.pagedownEditor.run();
    this.pagedownEditor.hooks.set('insertLinkDialog', (callback: Function) => {
      this.$marker.$store.dispatch('modal/open', {
        type: 'link',
        callback,
      });
      return true;
    });
    this.pagedownEditor.hooks.set('insertImageDialog', (callback: Function) => {
      this.$marker.$store.dispatch('modal/open', {
        type: 'image',
        callback,
      });
      return true;
    });
  }

  /**
   * Init inline images
   */
  initCoreImageCache() {

    const imgCache: { [key: string]: HTMLImageElement[] } = {};

    const hashImgEle = (imgEle: HTMLImageElement) => `${imgEle.src}:${imgEle.width || -1}:${imgEle.height || -1}`;

    const addToImgCache = (imgEle: HTMLImageElement) => {
      const hash = hashImgEle(imgEle);
      let entries = imgCache[hash];
      if (!entries) {
        entries = [];
        imgCache[hash] = entries;
      }
      entries.push(imgEle);
    };

    const getFromImgCache = (imgElesToCache: HTMLImageElement) => {
      const hash = hashImgEle(imgElesToCache);
      const entries = imgCache[hash];
      if (!entries) {
        return null;
      }
      let imgEle;
      return entries
        .some((entry: HTMLImageElement) => {
          if (this.editorEle.contains(entry)) {
            return false;
          }
          imgEle = entry;
          return true;
        }) && imgEle;
    };

    const triggerImgCacheGc = debounce(() => {
      Object.entries(imgCache).forEach(([src, entries]) => {
        // Filter entries that are not attached to the DOM
        const filteredEntries = entries.filter(imgEle => this.editorEle.contains(imgEle));
        if (filteredEntries.length) {
          imgCache[src] = filteredEntries;
        } else {
          delete imgCache[src];
        }
      });
    }, 100);

    let imgElesToCache: HTMLImageElement[] = [];
    if (this.settings.editor!.inlineImages) {
      this.core.$events.$on(CORE_SECTION_HIGHLIGHTED, (section: Section) => {
        Array.prototype.forEach.call(
          section.ele.getElementsByClassName('token img'),
          (imgTokenEle: HTMLImageElement) => {
            const srcEle = imgTokenEle.querySelector('.token.src');
            if (srcEle) {
              // Create an img element before the .img.token and wrap both elements
              // into a .token.img-wrapper
              const imgEle = document.createElement('img');
              imgEle.style.display = 'none';
              const uri = srcEle.textContent!;
              if (!/^unsafe/.test(sanitizeUri(uri, true))) {
                imgEle.onload = () => {
                  imgEle.style.display = '';
                };
                imgEle.src = uri;
                // Take img size into account
                const sizeEle = imgTokenEle.querySelector('.token.size');
                if (sizeEle) {
                  const match = sizeEle.textContent!.match(/=(\d*)x(\d*)/)!;
                  if (match[1]) {
                    imgEle.width = parseInt(match[1], 10);
                  }
                  if (match[2]) {
                    imgEle.height = parseInt(match[2], 10);
                  }
                }
                imgElesToCache.push(imgEle);
              }
              const imgTokenWrapper = document.createElement('span');
              imgTokenWrapper.className = 'token img-wrapper';
              imgTokenEle.parentNode!.insertBefore(imgTokenWrapper, imgTokenEle);
              imgTokenWrapper.appendChild(imgEle);
              imgTokenWrapper.appendChild(imgTokenEle);
            }
          },
        );
      });
    }

    this.core.$events.$on(CORE_HIGHLIGHTED, () => {
      imgElesToCache.forEach(imgEle => {
        const cachedImgEle = getFromImgCache(imgEle);
        if (cachedImgEle) {
          // Found a previously loaded image that has just been released
          imgEle.parentNode!.replaceChild(cachedImgEle, imgEle);
        } else {
          addToImgCache(imgEle);
        }
      });
      imgElesToCache = [];
      // Eject released images from cache
      triggerImgCacheGc();
    });
  }

  /**
   * Active sync action for tasklist.
   */
  activeSyncTaskList() {

    const getPreviewOffset = (ele: HTMLElement): number => {
      let offset = 0;
      if (!ele || ele === this.previewEle) {
        return offset;
      }
      let { previousSibling } = ele;
      while (previousSibling) {
        offset += previousSibling.textContent!.length;
        ({ previousSibling } = previousSibling);
      }
      return offset + getPreviewOffset(ele.parentNode as HTMLElement);
    };

    this.previewEle.addEventListener('click', (event: MouseEvent) => {
      if (!(event.target as HTMLElement).classList.contains('task-list-item-checkbox')) {
        return;
      }

      event.preventDefault();

      if (!this.$marker.$store.getters['content/isCurrentEditable']) {
        return;
      }

      const editorContent = this.core.getContent();

      // Use setTimeout to ensure evt.target.checked has the old value
      setTimeout(() => {
        // Make sure content has not changed
        if (editorContent !== this.core.getContent()) {
          return;
        }

        const previewOffset = getPreviewOffset(event.target as HTMLElement);
        const endOffset = this.getEditorOffset(previewOffset + 1);

        if (endOffset != null) {
          const startOffset = editorContent.lastIndexOf('\n', endOffset) + 1;
          const line = editorContent.slice(startOffset, endOffset);
          const match = line.match(/^([ \t]*(?:[*+-]|\d+\.)[ \t]+\[)[ xX](\] .*)/);
          if (match) {
            let newContent = editorContent.slice(0, startOffset);
            newContent += match[1];
            newContent += (event.target as HTMLInputElement).checked ? ' ' : 'x';
            newContent += match[2];
            newContent += editorContent.slice(endOffset);
            this.core.setContent(newContent, true);
          }
        }
      }, 10);

    });
  }

  /**
   * Active worker for sync selections from preview to editor.
   */
  activeSyncSelection() {
    if (this.syncSelectionActived) return;

    window.addEventListener('keyup', this.syncSelection.bind(this));
    window.addEventListener('mouseup', this.syncSelection.bind(this));
    window.addEventListener('focusin', this.syncSelection.bind(this));
    window.addEventListener('contextmenu', this.syncSelection.bind(this));
  }

  /**
   * Deactive worker for sync selections from preview to editor.
   */
  deactiveSyncSelection() {
    window.removeEventListener('keyup', this.syncSelection.bind(this));
    window.removeEventListener('mouseup', this.syncSelection.bind(this));
    window.removeEventListener('focusin', this.syncSelection.bind(this));
    window.removeEventListener('contextmenu', this.syncSelection.bind(this));
  }

  /**
   * Active preview refresher
   */
  activePreviewRefresher() {

    const refreshPreview = allowDebounce(() => {
      this.convert();
      if (instantPreview) {
        this.refreshPreview();
        this.measureSectionDimensions(true);
      } else {
        setTimeout(() => this.refreshPreview(), 10);
      }
      instantPreview = false;
    }, 25);

    let newSectionList: Section[];
    let newSelectionRange: Range;
    const onEditorChanged = allowDebounce(() => {
      if (this.sectionList !== newSectionList) {
        this.sectionList = newSectionList;
        this.$events.$emit(MEE_SECTION_LIST, this.sectionList);
        refreshPreview(!instantPreview);
      }
      if (this.selectionRange !== newSelectionRange) {
        this.selectionRange = newSelectionRange;
        this.$events.$emit(MEE_SELECTION_RANGE, this.selectionRange);
      }
    }, 10);

    this.core.$events.$on(CORE_SELECTION_CHANGED, (start: number, end: number, selectionRange: Range) => {
      newSelectionRange = selectionRange;
      onEditorChanged(!instantPreview);
    });

    this.core.$events.$on(CORE_CONTENT_CHANGED, (content: string, diffs: Diff[], sectionList: Section[]) => {
      newSectionList = sectionList;
      onEditorChanged(!instantPreview);
    });
  }

  /**
   * Finish the conversion initiated by the section parser
   */
  convert() {
    this.conversionCtx = convert(this.parsingCtx, this.conversionCtx);
    this.$events.$emit(MEE_CONVERSION_CTX, this.conversionCtx);
    ({ tokens } = this.parsingCtx.markdownState!);
  }

  /**
   * Refresh the preview with the result of `convert()`
   */
  async refreshPreview() {
    const sectionInfoList: SectionInfo[] = [];
    let sectionPreviewEle;
    let sectionTocEle;
    let sectionIdx = 0;
    let sectionInfoIdx = 0;
    let insertBeforePreviewEle = this.previewEle.firstChild!;
    let insertBeforeTocEle = this.tocEle.firstChild!;
    let previewHtml = '';
    let loadingImages: HTMLImageElement[] = [];

    this.conversionCtx.htmlSectionDiff!
      .forEach(item => {
        for (let i = 0; i < item[1].length; i += 1) {
          const section = this.conversionCtx.sectionList![sectionIdx];
          //  diff same
          if (item[0] === 0) {
            let sectionInfo = this.previewCtx.sectionInfoList![sectionInfoIdx];
            sectionInfoIdx += 1;
            if (sectionInfo.editorEle !== section.ele) {
              // Force textToPreviewDiffs computation
              sectionInfo = new SectionInfo(
                section,
                sectionInfo.previewEle,
                sectionInfo.tocEle,
                sectionInfo.html,
              );
            }
            sectionInfoList.push(sectionInfo);
            previewHtml += sectionInfo.html;
            sectionIdx += 1;
            insertBeforePreviewEle = insertBeforePreviewEle.nextSibling!;
            insertBeforeTocEle = insertBeforeTocEle.nextSibling!;
          } else if (item[0] === -1) {
            // diff front diff
            sectionInfoIdx += 1;
            sectionPreviewEle = insertBeforePreviewEle;
            insertBeforePreviewEle = insertBeforePreviewEle.nextSibling!;
            this.previewEle.removeChild(sectionPreviewEle);
            sectionTocEle = insertBeforeTocEle;
            insertBeforeTocEle = insertBeforeTocEle.nextSibling!;
            this.tocEle.removeChild(sectionTocEle);
          } else if (item[0] === 1) {
            // diff backend diff
            const html = sanitizeHtml(this.conversionCtx.htmlSectionList![sectionIdx], this.settings.sanitizeOptions);
            sectionIdx += 1;

            // Create preview section element
            sectionPreviewEle = document.createElement('div');
            sectionPreviewEle.className = 'preview-section';
            sectionPreviewEle.innerHTML = html;
            if (insertBeforePreviewEle) {
              this.previewEle.insertBefore(sectionPreviewEle, insertBeforePreviewEle);
            } else {
              this.previewEle.appendChild(sectionPreviewEle);
            }
            renderSection(sectionPreviewEle, this.options, true);
            loadingImages = [
              ...loadingImages,
              ...Array.prototype.slice.call(sectionPreviewEle.getElementsByTagName('img')),
            ];

            // Create TOC section element
            sectionTocEle = document.createElement('div');
            sectionTocEle.className = 'toc-section';
            const headingEle = sectionPreviewEle.querySelector('h1, h2, h3, h4, h5, h6');
            if (headingEle) {
              const clonedEle = headingEle.cloneNode(true);
              (clonedEle as HTMLElement).removeAttribute('id');
              sectionTocEle.appendChild(clonedEle);
            }
            if (insertBeforeTocEle) {
              this.tocEle.insertBefore(sectionTocEle, insertBeforeTocEle);
            } else {
              this.tocEle.appendChild(sectionTocEle);
            }

            previewHtml += html;
            sectionInfoList.push(new SectionInfo(section, sectionPreviewEle, sectionTocEle, html));
          }
        }
      });

    this.tocEle.classList[
      this.tocEle.querySelector('.toc-section *') ? 'remove' : 'add'
    ]('toc-tab--empty');

    this.previewCtx = {
      markdown: this.conversionCtx.text,
      html: previewHtml.replace(/^\s+|\s+$/g, ''),
      text: this.previewEle.textContent!,
      sectionInfoList,
    };

    this.$events.$emit(MEE_PREVIEW_CTX, this.previewCtx);
    this.makeTextToPreviewDiffs();

    // Wait for images to load
    const loadedPromises = loadingImages.map(imgEle => new Promise(resolve => {
      if (!imgEle.src) {
        resolve();
        return;
      }
      const img = new Image();
      img.onload = resolve;
      img.onerror = resolve;
      img.src = imgEle.src;
    }));
    await Promise.all(loadedPromises);

    // Debounce if sections have already been measured
    this.measureSectionDimensions();
  }

  /**
   * Measure the height of each section in editor, preview and toc.
   */
  measureSectionDimensionsWorker(force = false) {
    if (!force && this.previewCtx === this.previewCtxMeasured) {
      return;
    }
    measureSectionDimensions(this);
    this.previewCtxMeasured = this.previewCtx;
    this.$events.$emit(MEE_PREVIEW_CTX_MEASURED, this.previewCtxMeasured);
  }

  /**
   * Compute the diffs between editor's markdown and preview's html
   * asynchronously unless there is only one section to compute.
   */
  makeTextToPreviewDiffs() {
    if (this.previewCtx !== this.previewCtxWithDiffs) {
      const makeOne = () => {
        let hasOne = false;
        const hasMore = this.previewCtx.sectionInfoList!
          .some(sectionInfo => {
            if (!sectionInfo.textToPreviewDiffs) {
              if (hasOne) {
                return true;
              }
              if (!sectionInfo.previewText) {
                sectionInfo.previewText = sectionInfo.previewEle.textContent!;
              }
              sectionInfo.textToPreviewDiffs = diffMatchPatch.diff_main(
                sectionInfo.section.text,
                sectionInfo.previewText,
              );
              hasOne = true;
            }
            return false;
          });
        if (hasMore) {
          setTimeout(() => makeOne(), 10);
        } else {
          this.previewCtxWithDiffs = this.previewCtx;
          this.$events.$emit(MEE_PREVIEW_CTX_WITH_DIFFS, this.previewCtxWithDiffs);
        }
      };
      makeOne();
    }
  }

  /**
   * Report selection from the preview to the editor.
   */
  syncSelectionWorker() {
    const selection = window.getSelection()!;
    let range: any = selection.rangeCount && selection.getRangeAt(0);
    if (range) {
      if (
        /* eslint-disable no-bitwise */
        !(this.previewEle.compareDocumentPosition(range.startContainer)
          & Node.DOCUMENT_POSITION_CONTAINED_BY)
        || !(this.previewEle.compareDocumentPosition(range.endContainer)
        & Node.DOCUMENT_POSITION_CONTAINED_BY)
      /* eslint-enable no-bitwise */
      ) {
        range = null;
      }
    }

    if (this.previewSelectionRange !== range) {
      let previewSelectionStartOffset;
      let previewSelectionEndOffset;
      if (range) {
        const startRange = document.createRange();
        startRange.setStart(this.previewEle, 0);
        startRange.setEnd(range.startContainer, range.startOffset);
        previewSelectionStartOffset = `${startRange}`.length;
        previewSelectionEndOffset = previewSelectionStartOffset + `${range}`.length;
        const editorStartOffset = this.getEditorOffset(previewSelectionStartOffset);
        const editorEndOffset = this.getEditorOffset(previewSelectionEndOffset);
        if (editorStartOffset != null && editorEndOffset != null) {
          this.core.selectionMgr.setSelectionStartEnd(
            editorStartOffset,
            editorEndOffset,
          );
        }
      }
      this.previewSelectionRange = range;
      this.$events.$emit(MEE_PREVIEW_SELECTION_RANGE, this.previewSelectionRange);
    }
  }

  /**
   * Change editor content
   * @param text
   */
  applyContent(text: string) {
    const options = precessOptions(defaultProperties);
    if (serializeObject(options) !== serializeObject(this.options)) {
      this.options = options;
      this.initPrism();
      this.initConverter();
    }

    this.initCore();

    this.core.setContent(text, true);
  }

  /**
   * Initialize the markdown-it converter with the options
   */
  initConverter() {
    this.converter = createConverter(this.options, true);
  }

  /**
   * Initialize the Prism grammar with the options
   */
  initPrism() {
    const options = {
      ...this.options,
      insideFences: defaultOptions.insideFences,
    };
    this.prismGrammars = makeGrammars(options);
  }

  /**
   * Initialize the cledit editor with markdown-it section parser and Prism highlighter
   */
  initCore() {
    const that = this;
    this.previewCtxMeasured = null;
    this.$events.$emit(MEE_PREVIEW_CTX_MEASURED, null);
    this.previewCtxWithDiffs = null;
    this.$events.$emit(MEE_PREVIEW_CTX_WITH_DIFFS, null);
    const options: EditorOptions = {
      sectionHighlighter(section: Section) {
        return Prism.highlight(section.text, that.prismGrammars[section.data], '');
      },
      sectionParser(text: string) {
        that.parsingCtx = parseSections(that.converter, text);
        return that.parsingCtx.sections;
      },
      getCursorFocusRatio() {
        return 1;
      },
    };

    this.core.init(options);
  }
}

applyMixins(Editor, [EditorUtils]);
