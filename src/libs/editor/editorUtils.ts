import DiffMatchPatch, { Diff } from 'diff-match-patch';
import { Store } from 'vuex';
import { DimensionNames, EditorCtx } from '@/libs/editor/editor';
import { RootState } from '@/store';


// eslint-disable-next-line new-cap
const diffMatchPatch = new DiffMatchPatch.diff_match_patch();

export class EditorUtils {
  $store!: Store<RootState>;
  editorEle!: HTMLElement;
  previewEle!: HTMLElement;
  previewCtxMeasured!: EditorCtx | null;
  previewCtxWithDiffs!: EditorCtx | null;

  /**
   * Get an object describing the position of the scroll bar in the file.
   */
  getScrollPosition() {
    if (!this.previewCtxMeasured) {
      return false;
    }

    let scrollTop: number;
    let dimensionKey: DimensionNames;
    if (this.$store.getters['Layout/showEditor']) {
      dimensionKey = 'editorDimension';
      scrollTop = (this.editorEle.parentNode! as HTMLElement).scrollTop;
    } else {
      dimensionKey = 'previewDimension';
      scrollTop = (this.previewEle.parentNode! as HTMLElement).scrollTop;
    }

    let result;
    this.previewCtxMeasured.sectionInfoList!.some((item, index) => {
      if (scrollTop < item[dimensionKey].endOffset) {
        const posInSection = (scrollTop - item[dimensionKey].startOffset) / (item[dimensionKey].height || 1);
        result = { posInSection, sectionIdx: index };
        return true;
      }
      return false;
    });

    return result;
  }

  /**
   * Get the offset of the markdown in the editor corresponding to the offset in the preview
   */
  getEditorOffset(
    previewOffset: number,
    sectionInfoList = (this.previewCtxWithDiffs || {}).sectionInfoList,
  ) {
    if (!sectionInfoList) {
      return null;
    }
    let offset = previewOffset;
    let editorOffset = 0;
    sectionInfoList.some(sectionInfo => {
      if (!sectionInfo.textToPreviewDiffs) {
        editorOffset = 0;
        return true;
      }

      if (sectionInfo.previewText.length >= offset) {
        const previewToTextDiffs = sectionInfo.textToPreviewDiffs
          .map((diff: Diff) => [-diff[0], diff[1]]);

        editorOffset += diffMatchPatch.diff_xIndex(previewToTextDiffs as Diff[], offset);
        return true;
      }

      offset -= sectionInfo.previewText.length;
      editorOffset += sectionInfo.section.text.length;
      return false;
    });
    return editorOffset;
  }
}
