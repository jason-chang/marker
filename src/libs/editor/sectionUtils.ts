import { DimensionNames, Editor } from '@/libs/editor/editor';


export class SectionDimension {
  height: number;
  constructor(
    public startOffset: number,
    public endOffset: number,
  ) {
    this.height = endOffset - startOffset;
  }
}

function dimensionNormalizer(dimensionName: DimensionNames) {
  return (editorSvc: Editor) => {
    const dimensionList = editorSvc.previewCtx.sectionInfoList!
      .map(sectionInfo => sectionInfo[dimensionName]);
    let dimension;
    let i;
    let j;
    for (i = 0; i < dimensionList.length; i += 1) {
      dimension = dimensionList[i];
      if (dimension.height) {
        for (j = i + 1; j < dimensionList.length && dimensionList[j].height === 0; j += 1) {
          // Loop
        }
        const normalizeFactor = j - i;
        if (normalizeFactor !== 1) {
          const normalizedHeight = dimension.height / normalizeFactor;
          dimension.height = normalizedHeight;
          dimension.endOffset = dimension.startOffset + dimension.height;
          for (j = i + 1; j < i + normalizeFactor; j += 1) {
            const startOffset = dimension.endOffset;
            dimension = dimensionList[j];
            dimension.startOffset = startOffset;
            dimension.height = normalizedHeight;
            dimension.endOffset = dimension.startOffset + dimension.height;
          }
          i = j - 1;
        }
      }
    }
  };
}

const normalizeEditorDimensions = dimensionNormalizer('editorDimension');
const normalizePreviewDimensions = dimensionNormalizer('previewDimension');
const normalizeTocDimensions = dimensionNormalizer('tocDimension');

export function measureSectionDimensions(editorSvc: Editor) {
  let editorSectionOffset = 0;
  let previewSectionOffset = 0;
  let tocSectionOffset = 0;
  let sectionInfo = editorSvc.previewCtx.sectionInfoList![0];
  let nextSectionDesc;
  let i = 1;
  for (; i < editorSvc.previewCtx.sectionInfoList!.length; i += 1) {
    nextSectionDesc = editorSvc.previewCtx.sectionInfoList![i];

    // Measure editor section
    let newEditorSectionOffset = nextSectionDesc.editorEle
      ? nextSectionDesc.editorEle.offsetTop
      : editorSectionOffset;
    newEditorSectionOffset = newEditorSectionOffset > editorSectionOffset
      ? newEditorSectionOffset
      : editorSectionOffset;
    sectionInfo.editorDimension = new SectionDimension(
      editorSectionOffset,
      newEditorSectionOffset,
    );
    editorSectionOffset = newEditorSectionOffset;

    // Measure preview section
    let newPreviewSectionOffset = nextSectionDesc.previewEle
      ? nextSectionDesc.previewEle.offsetTop
      : previewSectionOffset;
    newPreviewSectionOffset = newPreviewSectionOffset > previewSectionOffset
      ? newPreviewSectionOffset
      : previewSectionOffset;
    sectionInfo.previewDimension = new SectionDimension(
      previewSectionOffset,
      newPreviewSectionOffset,
    );
    previewSectionOffset = newPreviewSectionOffset;

    // Measure TOC section
    let newTocSectionOffset = nextSectionDesc.tocEle
      ? nextSectionDesc.tocEle.offsetTop + (nextSectionDesc.tocEle.offsetHeight / 2)
      : tocSectionOffset;
    newTocSectionOffset = newTocSectionOffset > tocSectionOffset
      ? newTocSectionOffset
      : tocSectionOffset;
    sectionInfo.tocDimension = new SectionDimension(tocSectionOffset, newTocSectionOffset);
    tocSectionOffset = newTocSectionOffset;

    sectionInfo = nextSectionDesc;
  }

  // Last section
  sectionInfo = editorSvc.previewCtx.sectionInfoList![i - 1];
  if (sectionInfo) {
    sectionInfo.editorDimension = new SectionDimension(
      editorSectionOffset,
      editorSvc.editorEle.scrollHeight,
    );
    sectionInfo.previewDimension = new SectionDimension(
      previewSectionOffset,
      editorSvc.previewEle.scrollHeight,
    );
    sectionInfo.tocDimension = new SectionDimension(
      tocSectionOffset,
      editorSvc.tocEle.scrollHeight,
    );
  }

  normalizeEditorDimensions(editorSvc);
  normalizePreviewDimensions(editorSvc);
  normalizeTocDimensions(editorSvc);
}
