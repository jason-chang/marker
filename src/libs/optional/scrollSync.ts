
import { EditorCtx, Editor, MEE_PREVIEW_CTX, MEE_PREVIEW_CTX_MEASURED, MEE_SECTION_LIST, SectionInfo } from '@/libs/editor/editor';
import { animate, animationSetter } from '@/libs/animation';

let throttleTimeoutId: any;
let throttleLastTime = 0;

function throttle(func: Function, wait: number) {
  clearTimeout(throttleTimeoutId);
  const currentTime = Date.now();
  const localWait = (wait + throttleLastTime) - currentTime;
  if (localWait < 1) {
    throttleLastTime = currentTime;
    func();
  } else {
    throttleTimeoutId = setTimeout(() => {
      throttleLastTime = Date.now();
      func();
    }, localWait);
  }
}

class ScrollSyncer {
  editorFinishTimeoutId!: number;
  previewFinishTimeoutId!: number;

  isScrollEditor!: boolean;
  isScrollPreview!: boolean;

  isEditorMoving!: boolean;
  isPreviewMoving!: boolean;

  sectionInfoList: SectionInfo[] = [];

  isPreviewRefreshing!: boolean;
  timeoutId!: number;

  get editorScrollerEle() {
    return this.editor.editorEle.parentNode as HTMLElement;
  }

  get previewScrollerEle() {
    return this.editor.previewEle.parentNode as HTMLElement;
  }

  get localSkipAnimation() {
    return !this.editor.$store.getters['Layout/showPreview'];
  }

  constructor(
    public editor: Editor,
  ) {
    this.listenScrollEvents();
    this.listenEditorEvent();
    this.watchSyncScrollState();
  }

  listenScrollEvents() {
    (this.editor.editorEle.parentNode as HTMLElement)
      .addEventListener('scroll', () => {
        if (this.isEditorMoving) {
          return;
        }
        this.isScrollEditor = true;
        this.isScrollPreview = false;
        this.doScrollSync();
      });

    (this.editor.previewEle.parentNode as HTMLElement)
      .addEventListener('scroll', () => {
        if (this.isPreviewMoving || this.isPreviewRefreshing) {
          return;
        }
        this.isScrollPreview = true;
        this.isScrollEditor = false;
        this.doScrollSync();
      });
  }

  listenEditorEvent() {
    this.editor.$events.$on(MEE_SECTION_LIST, () => {
      clearTimeout(this.timeoutId);
      this.isPreviewRefreshing = true;
      this.sectionInfoList = [];
    });

    this.editor.$events.$on(MEE_PREVIEW_CTX, () => {
      // Assume the user is writing in the editor
      this.isScrollEditor = this.editor.$store.getters['Layout/showEditor'];
      // A preview scrolling event can occur if height is smaller
      this.timeoutId = window.setTimeout(() => {
        this.isPreviewRefreshing = false;
      }, 100);
    });

    this.editor.$events.$on(MEE_PREVIEW_CTX_MEASURED, (previewCtxMeasured: EditorCtx) => {
      if (previewCtxMeasured) {
        this.sectionInfoList = previewCtxMeasured.sectionInfoList!;
        this.forceScrollSync();
      }
    });
  }

  watchSyncScrollState() {
    this.editor.$marker.$store.watch(
      () => this.editor.$store.state.Layout!.scrollSync,
      () => this.forceScrollSync(),
    );

    this.editor.$marker.$store.watch(
      () => this.editor.$store.state.Layout!.onlyEditor,
      (showEditor: boolean) => {
        this.isScrollEditor = showEditor;
        this.isScrollPreview = !showEditor;
      },
    );

    this.editor.$marker.$store.watch(
      () => this.editor.$store.state.Layout!.onlyPreview,
      (showEditor: boolean) => {
        this.isScrollEditor = showEditor;
        this.isScrollPreview = !showEditor;
      },
    );
  }

  doScrollSync() {
    if (!this.editor.$store.state.Layout!.scrollSync || this.sectionInfoList.length === 0) {
      return;
    }

    if (this.isScrollEditor) {
      this.syncToPreview();
    } else if (!this.editor.$store.getters['Layout/showEditor'] || this.isScrollPreview) {
      this.syncToEditor();
    }
  }

  /**
   * Sync scroll top from side editor to side preview
   */
  syncToPreview() {
    let editorScrollTop = this.editorScrollerEle.scrollTop;
    if (editorScrollTop < 0) {
      editorScrollTop = 0;
    }
    let scrollTo: number = 0;


    this.isScrollEditor = false;
    this.sectionInfoList.some(sectionInfo => {
      if (editorScrollTop > sectionInfo.editorDimension.endOffset) {
        return false;
      }
      const posInSection = (editorScrollTop - sectionInfo.editorDimension.startOffset)
        / (sectionInfo.editorDimension.height || 1);
      scrollTo = (sectionInfo.previewDimension.startOffset
        + (sectionInfo.previewDimension.height * posInSection));
      return true;
    });

    scrollTo = Math.min(
      scrollTo,
      this.previewScrollerEle.scrollHeight - this.previewScrollerEle.offsetHeight,
    );

    throttle(() => {
      clearTimeout(this.previewFinishTimeoutId);

      animate(this.previewScrollerEle)
        .scrollTop(scrollTo)
        .duration(!this.localSkipAnimation && 100)
        .start(
          () => {
            this.previewFinishTimeoutId = window.setTimeout(() => {
              this.isPreviewMoving = false;
            }, 100);
          },
          () => {
            this.isPreviewMoving = true;
          },
        );
    }, this.localSkipAnimation ? 500 : 50);
  }

  /**
   * Sync scroll top from side preview to side editor
   */
  syncToEditor() {
    const previewScrollTop = this.previewScrollerEle.scrollTop;
    let scrollTo: number = 0;

    this.isScrollPreview = false;
    this.sectionInfoList.some(sectionInfo => {
      if (previewScrollTop > sectionInfo.previewDimension.endOffset) {
        return false;
      }
      const posInSection = (previewScrollTop - sectionInfo.previewDimension.startOffset)
        / (sectionInfo.previewDimension.height || 1);
      scrollTo = (sectionInfo.editorDimension.startOffset
        + (sectionInfo.editorDimension.height * posInSection));
      return true;
    });

    scrollTo = Math.min(
      scrollTo,
      this.editorScrollerEle.scrollHeight - this.editorScrollerEle.offsetHeight,
    );

    throttle(() => {
      clearTimeout(this.editorFinishTimeoutId);
      animate(this.editorScrollerEle)
        .scrollTop(scrollTo)
        .duration(!this.localSkipAnimation && 100)
        .start(() => {
          this.editorFinishTimeoutId = window.setTimeout(() => {
            this.isEditorMoving = false;
          }, 100);
        }, () => {
          this.isEditorMoving = true;
        });
    }, this.localSkipAnimation ? 500 : 50);
  }

  forceScrollSync() {
    if (!this.isPreviewRefreshing) {
      this.doScrollSync();
    }
  }
}

export function activeScrollSync(editor: Editor) {
  return new ScrollSyncer(editor);
}
