import Mousetrap from 'mousetrap';
import { Editor } from '@/libs/editor/editor';
import { EditActions } from '@/conf/settings';

// Skip shortcuts if modal is open or editor is hidden
Mousetrap.prototype.stopCallback = () => true;


export class ShortcutManager {

  shortcutMethods!: { [key: string]: Function };

  constructor(
    public editor: Editor,
  ) {
    this.createActions();
  }

  active() {
    Mousetrap.prototype.stopCallback = () => false;
    this.register();
  }

  deactive() {
    this.unRegister();
    Mousetrap.prototype.stopCallback = () => true;
  }

  pagedownHandler(name: string) {
    return () => {
      this.editor.pagedownEditor.uiManager.doClick(name);
      return true;
    };
  }

  findReplaceOpener(type: string) {
    return () => {
      this.editor!.$store.dispatch('FindReplace/open', {
        type,
        findText: this.editor.core.selectionMgr.hasFocus()
          && this.editor.core.selectionMgr.getSelectedText(),
      });
      return true;
    };
  }

  createActions() {
    const that = this;
    this.shortcutMethods = {
      bold: this.pagedownHandler('bold'),
      italic: this.pagedownHandler('italic'),
      strikethrough: this.pagedownHandler('strikethrough'),
      link: this.pagedownHandler('link'),
      quote: this.pagedownHandler('quote'),
      code: this.pagedownHandler('code'),
      image: this.pagedownHandler('image'),
      olist: this.pagedownHandler('olist'),
      ulist: this.pagedownHandler('ulist'),
      clist: this.pagedownHandler('clist'),
      heading: this.pagedownHandler('heading'),
      hr: this.pagedownHandler('hr'),
      find: this.findReplaceOpener('find'),
      replace: this.findReplaceOpener('replace'),
    };
  }

  expand(param1: string, param2: string) {
    const text = `${param1 || ''}`;
    const replacement = `${param2 || ''}`;
    if (text && replacement) {
      setTimeout(() => {
        const { selectionMgr } = this.editor.core;
        let offset = selectionMgr.selectionStart;
        if (offset === selectionMgr.selectionEnd) {
          const range = selectionMgr.createRange(offset - text.length, offset);
          if (`${range}` === text) {
            range.deleteContents();
            range.insertNode(document.createTextNode(replacement));
            offset = (offset - text.length) + replacement.length;
            selectionMgr.setSelectionStartEnd(offset, offset);
            selectionMgr.updateCursorCoordinates(true);
          }
        }
      }, 1);
    }
  }

  unRegister() {
    Mousetrap.reset();
  }

  register() {
    this.unRegister();
    this.registerShortcuts();
    this.registerExpands();
  }

  registerShortcuts() {
    Object
      .entries(this.editor.settings.shortcuts!)
      .forEach(([key, method]) => {
        if (!method) return;

        if (Object.prototype.hasOwnProperty.call(this.shortcutMethods, method)) {
          Mousetrap.bind(`${key}`, () => !this.shortcutMethods[method].call(this));
        }
      });
  }

  registerExpands() {
    Object
      .entries(this.editor.settings.expands!)
      .forEach(([key, expand]) => {
        if (!expand) return;

        Mousetrap.bind(`${key}`, () => this.expand(...expand.params));
      });
  }
}
