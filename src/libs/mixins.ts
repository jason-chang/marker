
import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import Clipboard from 'clipboard';
import merge from 'lodash/merge';

import { getSettings, Settings } from '@/conf/settings';

function setVisible(el: HTMLElement, value?: string) {
  el.style.display = value ? '' : 'none';
  if (value) {
    el.removeAttribute('aria-hidden');
  } else {
    el.setAttribute('aria-hidden', 'true');
  }
}

function setElTitle(el: HTMLElement, title: string = '') {
  el.title = title;
  el.setAttribute('aria-label', title);
}

// Clipboard directive
function createClipboard(el: HTMLElement, value: string) {
  el.clipboard = new Clipboard(el as Element, { text: () => value });
}

function destroyClipboard(el: HTMLElement) {
  if (el.clipboard) {
    el.clipboard.destroy();
    el.clipboard = undefined;
  }
}

@Component({
  directives: {
    focus: {
      inserted(el: HTMLElement) {
        el.focus();
        const { value } = (el as HTMLTextAreaElement);
        if (value && (el as HTMLTextAreaElement).setSelectionRange) {
          (el as HTMLTextAreaElement).setSelectionRange(0, value.length);
        }
      },
    },
    show: {
      bind(el: HTMLElement, { value }) {
        setVisible(el, value);
      },
      update(el: HTMLElement, { value, oldValue }) {
        if (value !== oldValue) {
          setVisible(el, value);
        }
      },
    },
    title: {
      bind(el: HTMLElement, { value }) {
        setElTitle(el, value);
      },
      update(el: HTMLElement, { value, oldValue }) {
        if (value !== oldValue) {
          setElTitle(el, value);
        }
      },
    },
    clipboard: {
      bind(el: HTMLElement, { value }) {
        createClipboard(el, value);
      },
      update(el: HTMLElement, { value, oldValue }) {
        if (value !== oldValue) {
          destroyClipboard(el);
          createClipboard(el, value);
        }
      },
      unbind(el: HTMLElement) {
        destroyClipboard(el);
      },
    },
  },
})
export class MarkerMixins extends Vue {

  @Prop({ type: Object, default: () => null })
  readonly settings!: Settings;

  beforeCreate() {
    const options = this.$options;
    // store injection
    if (options.events) {
      this.$events = options.events;
    } else if (options.parent && options.parent.$store) {
      this.$events = options.parent.$events;
    }
  }

  get $editor() {
    return this.editor || this.$parent.$editor;
  }

  get userSettings() {
    return merge(
      getSettings(),
      this.settings || (this.$parent && (this.$parent as MarkerMixins).settings),
    );
  }
}
