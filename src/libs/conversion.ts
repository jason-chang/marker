/* eslint-disable camelcase, new-cap */
import MarkdownIt from 'markdown-it';
import StateCore from 'markdown-it/lib/rules_core/state_core';
import Token from 'markdown-it/lib/token';
import { Diff, diff_match_patch } from 'diff-match-patch';
import Prism, { Grammar } from 'prismjs';
import { Rule } from 'markdown-it/lib';

import { precessOptions, initConverter } from '@/libs/converter';
import { makeGrammars } from '@/libs/grammar';
import { Section } from '@/core/section';
import { ConverterOptions } from '@/libs/converter/utils';
import presets from '@/conf/presets';


export interface TokenExt extends Token {
  sectionDelimiter: boolean;
}

const htmlSectionMarker = '\uF111\uF222\uF333\uF444';
const diffMatchPatch = new diff_match_patch();

// Create aliases for syntax highlighting
const languageAliases = ({
  html: 'markup',
  js: 'javascript',
  json: 'javascript',
  php: 'php',
  ps1: 'powershell',
  psm1: 'powershell',
  py: 'python',
  rb: 'ruby',
  svg: 'markup',
  ts: 'typescript',
  xml: 'markup',
  yml: 'yaml',
});

Object.entries(languageAliases).forEach(([alias, language]) => {
  Prism.languages[alias] = Prism.languages[language];
});

// Add programming language parsing capability to markdown fences
// @todo fix type hit
const insideFences: {
  [key: string]: {
    pattern: RegExp,
    inside: {
      rest: Grammar,
      [key: string]: RegExp | any,
    },
  },
} = {};

Object.entries(Prism.languages).forEach(([name, language]) => {
  if (Prism.util.type(language) === 'Object') {
    insideFences[`language-${name}`] = {
      pattern: new RegExp(`(\`\`\`|~~~)${name}\\W[\\s\\S]*`),
      inside: {
        'cl pre': /(```|~~~).*/,
        rest: language,
      },
    };
  }
});

// Disable spell checking in specific tokens
const noSpellcheckTokens = Object.create(null);
['code', 'pre', 'pre gfm', 'math block', 'math inline', 'math expr block', 'math expr inline', 'latex block']
  .forEach(key => { noSpellcheckTokens[key] = true; });

Prism.hooks.add('wrap', env => {
  if (noSpellcheckTokens[env.type]) {
    env.attributes.spellcheck = 'false';
  }
});

function createFlagMap(arr: string[]): { [key: string]: boolean } {
  return arr.reduce((map, type) => ({ ...map, [type]: true }), {});
}

const startSectionBlockTypeMap = createFlagMap([
  'paragraph_open', 'blockquote_open', 'heading_open', 'code', 'fence', 'table_open',
  'html_block', 'bullet_list_open', 'ordered_list_open', 'hr', 'dl_open',
]);

const listBlockTypeMap = createFlagMap([
  'bullet_list_open',
  'ordered_list_open',
]);

const blockquoteBlockTypeMap = createFlagMap([
  'blockquote_open',
]);

const tableBlockTypeMap = createFlagMap([
  'table_open',
]);
const deflistBlockTypeMap = createFlagMap([
  'dl_open',
]);

function hashArray(arr: string[], valueHash: { [key: string]: number }, valueArray: string[]) {
  const hash: number[] = [];

  arr.forEach(str => {
    let strHash = valueHash[str];
    if (strHash === undefined) {
      strHash = valueArray.length;
      valueArray.push(str);
      valueHash[str] = strHash;
    }
    hash.push(strHash);
  });

  return String.fromCharCode.apply(null, hash);
}

export const defaultProperties = { extensions: presets.default };

/**
 * Creates a converter and init it with extensions.
 * @returns {Object} A converter.
 */
export function createConverter(options: object, is: boolean = false) {
  // Let the listeners add the rules
  const converter = new MarkdownIt('default');
  converter.core.ruler.enable([], true);
  converter.block.ruler.enable([], true);
  converter.inline.ruler.enable([], true);
  initConverter(converter, options);
  Object.keys(startSectionBlockTypeMap).forEach(type => {
    const rule = converter.renderer.rules[type] || converter.renderer.renderToken;
    converter.renderer.rules[type] = (tokens, idx, opts, env, self) => {
      if ((tokens[idx] as TokenExt).sectionDelimiter) {
        // Add section delimiter
        return htmlSectionMarker + rule.call(converter.renderer, tokens, idx, opts, env, self);
      }
      return rule.call(converter.renderer, tokens, idx, opts, env, self);
    };
  });
  return converter;
}

// Default options for the markdown converter and the grammar
export const defaultOptions: ConverterOptions = {
  ...precessOptions(defaultProperties),
  insideFences,
};

export const defaultConverter = createConverter(defaultOptions);
export const defaultPrismGrammars = makeGrammars(defaultOptions);


export interface ParseingCtx {
  text: string,

  markdownCoreRules?: MarkdownIt.Rule[],
  sections?: { text: string, data: string }[],
  sectionList?: Section[],
  htmlSectionList?: string[],
  htmlSectionDiff?: Diff[],
  converter?: MarkdownIt,
  markdownState?: StateCore,
}

/**
 * Parse markdown sections by passing the 2 first block rules of the markdown-it converter.
 * @param {Object} converter The markdown-it converter.
 * @param {String} text The text to be parsed.
 * @returns {Object} A parsing context to be passed to `convert`.
 */
export function parseSections(converter: MarkdownIt, text: string) {
  // @todo fix typehit
  // @ts-ignore
  const markdownState = new StateCore(text, converter, {});

  converter.core.ruler.getRules('')
    .forEach((rule: Rule) => {
      rule(markdownState);
    });

  const lines = text.split('\n');
  if (!lines[lines.length - 1]) {
    // In cledit, last char is always '\n'.
    // Remove it as one will be added by addSection
    lines.pop();
  }
  const parsingCtx: ParseingCtx = {
    text,
    sections: [],
    converter,
    markdownState,
    // markdownCoreRules,
  };

  let data = 'main';
  let i = 0;

  function addSection(maxLine: number) {
    const section = { text: '', data };
    for (; i < maxLine; i += 1) {
      section.text += `${lines[i]}\n`;
    }
    if (section) {
      parsingCtx.sections!.push(section);
    }
  }

  markdownState.tokens.forEach((token: Token, index: number) => {
    // index === 0 means there are empty lines at the begining of the file
    if (token.level !== 0 || !startSectionBlockTypeMap[token.type]) {
      return;
    }

    if (index > 0) {
      (token as TokenExt).sectionDelimiter = true;
      addSection(token.map[0]);
    }

    if (listBlockTypeMap[token.type]) {
      data = 'list';
    } else if (blockquoteBlockTypeMap[token.type]) {
      data = 'blockquote';
    } else if (tableBlockTypeMap[token.type]) {
      data = 'table';
    } else if (deflistBlockTypeMap[token.type]) {
      data = 'deflist';
    } else {
      data = 'main';
    }
  });

  addSection(lines.length);

  return parsingCtx;
}

/**
 * Convert markdown sections previously parsed with `parseSections`.
 * @param {Object} parsingCtx The parsing context returned by `parseSections`.
 * @param {Object} previousConversionCtx The conversion context returned by a previous call
 * to `convert`, in order to calculate the `htmlSectionDiff` of the returned conversion context.
 * @returns {Object} A conversion context.
 */
export function convert(parsingCtx: ParseingCtx, previousConversionCtx: ParseingCtx): ParseingCtx {
  // // This function can be called twice without editor modification
  // // so prevent from converting it again.
  // if (!parsingCtx.markdownState!.isConverted) {
  //   // Skip 2 first rules previously passed in parseSections
  //   parsingCtx.markdownCoreRules!.slice(2).forEach(rule => rule(parsingCtx.markdownState!));
  //   parsingCtx.markdownState!.isConverted = true;
  // }

  const { tokens } = parsingCtx.markdownState!;
  const html = parsingCtx.converter!.renderer.render(
    tokens,
    parsingCtx.converter!.options,
    parsingCtx.markdownState!.env,
  );

  const htmlSectionList = html.split(htmlSectionMarker);
  if (htmlSectionList[0] === '') {
    htmlSectionList.shift();
  }

  const valueHash = {};
  const valueArray: string[] = [];
  const newSectionHash = hashArray(htmlSectionList, valueHash, valueArray);

  let htmlSectionDiff: Diff[];
  if (previousConversionCtx) {
    const oldSectionHash = hashArray(previousConversionCtx.htmlSectionList!, valueHash, valueArray);
    htmlSectionDiff = diffMatchPatch.diff_main(oldSectionHash, newSectionHash, false);
  } else {
    htmlSectionDiff = [[1, newSectionHash]];
  }

  return {
    text: parsingCtx.text,
    sectionList: parsingCtx.sectionList,
    htmlSectionList,
    htmlSectionDiff,
  };
}

/**
 * Helper to highlight arbitrary markdown
 * @param {Object} markdown The markdown content to highlight.
 * @param {Object} converter An optional converter.
 * @param {Object} grammars Optional grammars.
 * @returns {Object} The highlighted markdown in HTML format.
 */
export function highlight(markdown: string, converter = defaultConverter, grammars = defaultPrismGrammars) {
  const parsingCtx = parseSections(converter, markdown);
  return parsingCtx.sections!
    .map(section => Prism.highlight(section.text, grammars[section.data], ''))
    .join('');
}
