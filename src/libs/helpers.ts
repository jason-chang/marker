
export function ArrHasBy<T>(
  arr: Array<T>,
  cb: (item: any, index: number, all: Array<T>) => boolean,
) {
  for (let index = 0; index < arr.length; index++) {
    if (cb(arr[index], index, arr)) {
      return true;
    }
  }
}

export function functionBindWith(
  func: Function,
  context: any = null,
  ...withArgs: any[]
) {
  return (...args: any[]) => func.apply(context, withArgs.concat(args));
}

export function mapValuesUntil(obj: { [key: string]: any }, cb: (value: any, key: string, obj: object) => boolean) {
  const keys = Object.keys(obj);
  for (let i = 0; i < keys.length; i++) {
    if (cb(obj[keys[i]], keys[i], obj)) {
      return true;
    }
  }
  return false;
}
