import katex from 'katex';
import MarkdownIt from 'markdown-it';
import markdownItMath from '../libs/markdownItMath';
import { registerOptionProcesser, registerConverterInitializer, registerSectionRender, ConverterOptions, Properties } from '@/libs/converter/utils';


registerOptionProcesser((options: ConverterOptions, properties: Properties) => {
  options.math = properties.extensions!.katex.enabled;
});

registerConverterInitializer(2, (markdown: MarkdownIt, options: ConverterOptions) => {
  if (options.math) {
    markdown.use(markdownItMath);
    markdown.renderer.rules.inline_math = (tokens, idx) => `<span class="katex--inline">${markdown.utils.escapeHtml(tokens[idx].content)}</span>`;
    markdown.renderer.rules.display_math = (tokens, idx) => `<span class="katex--display">${markdown.utils.escapeHtml(tokens[idx].content)}</span>`;
  }
});

registerSectionRender((ele: HTMLElement) => {
  function highlighter(displayMode: boolean) {
    return (katexEle: HTMLElement) => {
      if (!katexEle.highlighted) {
        try {
          katex.render(katexEle.textContent!, katexEle, { displayMode });
        } catch (e) {
          katexEle.textContent = `${e.message}`;
        }
      }
      katexEle.highlighted = true;
    };
  }

  ele.querySelectorAll('.katex--inline').forEach(highlighter(false) as any);
  ele.querySelectorAll('.katex--display').forEach(highlighter(true) as any);
});
