import MarkdownIt from 'markdown-it';
import markdownItEmoji from 'markdown-it-emoji';
import { registerOptionProcesser, registerConverterInitializer, ConverterOptions, Properties } from '@/libs/converter/utils';

registerOptionProcesser((options: ConverterOptions, properties: Properties) => {
  options.emoji = properties.extensions!.emoji.enabled;
  options.emojiShortcuts = properties.extensions!.emoji.shortcuts;
});

registerConverterInitializer(1, (markdown: MarkdownIt, options: ConverterOptions) => {
  if (options.emoji) {
    markdown.use(markdownItEmoji, options.emojiShortcuts ? {} : { shortcuts: {} });
  }
});
