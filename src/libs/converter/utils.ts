import MarkdownIt from 'markdown-it';

const optionProcessers: Function[] = [];
const converterInitializers: Function[] = [];
const sectionRenders: Function[] = [];

export interface ConverterOptions {
  emoji?: boolean,
  emojiShortcuts?: boolean,
  math?: boolean,
  breaks?: boolean,
  linkify?: boolean,
  typographer?: boolean,
  fence?: boolean,
  table?: boolean,
  del?: boolean,
  abbr?: boolean,
  deflist?: boolean,
  footnote?: boolean,
  imgsize?: boolean,
  mark?: boolean,
  sub?: boolean,
  sup?: boolean,
  tasklist?: boolean,
  insideFences?: {
    [key: string]: object,
  },
}

export interface Properties {
  extensions?: {
    [key: string]: any
  }
}

// =----= Options =----=
export function registerOptionProcesser(cb: Function) {
  optionProcessers.push(cb);
}

export function precessOptions(properties: Properties, isCurrentFile: boolean = false) {
  return optionProcessers.reduce((options: ConverterOptions, listener: Function) => {
    listener(options, properties, isCurrentFile);
    return options;
  }, {});
}

// =----= Initializers =----=
export function registerConverterInitializer(priority: number, cb: Function) {
  converterInitializers[priority] = cb;
}

export function initConverter(markdown: MarkdownIt, options: object) {
  // Use forEach as it's a sparsed array
  converterInitializers.forEach(listener => {
    listener(markdown, options);
  });
}

// =----= Renders =----=
export function registerSectionRender(cb: Function) {
  sectionRenders.push(cb);
}

export function renderSection(ele: HTMLElement, options: object, isEditor: boolean) {
  sectionRenders.forEach(listener => {
    listener(ele, options, isEditor);
  });
}
