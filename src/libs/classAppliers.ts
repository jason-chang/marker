import { Marker } from '@/core';
import { debounce } from '@/core/utils';
import { unwrapRange, wrapRange } from '@/libs/utils';
import { CORE_CONTENT_CHANGED } from '@/core/events';
import { Editor } from '@/libs/editor/editor';

export class TickCaller {
  savedSelectionSwitch = false;
  nextTickCbs: Function[] = [];
  nextTickCbsCaller: Function;
  constructor(
    public editor: Editor,
  ) {
    this.nextTickCbsCaller = debounce(this.nextTickCbsCallerWorker.bind(this));
  }

  nextTickCbsCallerWorker() {
    while (this.nextTickCbs.length) {
      this.nextTickCbs.shift()!();
    }

    if (this.savedSelectionSwitch) {
      this.editor.core.selectionMgr.setSelectionStartEnd(
        this.editor.core.selectionMgr.selectionStart,
        this.editor.core.selectionMgr.selectionEnd,
      );
    }

    this.savedSelectionSwitch = false;
  }


  nextTick(cb: Function) {
    this.nextTickCbs.push(cb);
    this.nextTickCbsCaller();
  }

  nextTickRestoreSelection() {
    this.savedSelectionSwitch = true;
    this.nextTickCbsCaller();
  }
}

export class EditorClassApplier {
  classGetter: Function;
  offsetGetter: Function;
  properties: object;
  eleCollection: HTMLCollectionOf<Element>;
  lastEleCount: number;
  stopped: boolean = false;

  constructor(
    public editor: Editor,
    public tickCaller: TickCaller,
    classGetter: Function | string | string[],
    offsetGetter: Function | string,
    properties?: object,
  ) {
    this.classGetter = typeof classGetter === 'function' ? classGetter : () => classGetter;
    this.offsetGetter = typeof offsetGetter === 'function' ? offsetGetter : () => offsetGetter;
    this.properties = properties || {};
    this.eleCollection = editor.editorEle.getElementsByClassName(this.classGetter()[0]);
    this.lastEleCount = this.eleCollection.length;

    editor.core.$events.$on(CORE_CONTENT_CHANGED, this.restoreClass);
    tickCaller.nextTick(() => this.restoreClass());
  }

  restoreClass() {
    if (!this.eleCollection.length || this.eleCollection.length !== this.lastEleCount) {
      this.removeClass();
      this.applyClass();
    }
  }

  applyClass() {
    if (!this.stopped) {
      const offset = this.offsetGetter();
      if (offset && offset.start !== offset.end) {
        const range = this.editor.core.selectionMgr.createRange(
          Math.min(offset.start, offset.end),
          Math.max(offset.start, offset.end),
        );
        const properties = {
          ...this.properties,
          class: this.classGetter().join(' '),
        };

        this.editor.core.watcher.noWatch(() => {
          wrapRange(range, properties);
        });

        if (this.editor.core.selectionMgr.hasFocus()) {
          this.tickCaller.nextTickRestoreSelection();
        }
        this.lastEleCount = this.eleCollection.length;
      }
    }
  }

  removeClass() {
    this.editor.core.watcher.noWatch(() => {
      unwrapRange(this.eleCollection);
    });
    if (this.editor.core.selectionMgr.hasFocus()) {
      this.tickCaller.nextTickRestoreSelection();
    }
  }

  stop() {
    this.editor.core.$events.$off(CORE_CONTENT_CHANGED, this.restoreClass);
    this.tickCaller.nextTick(() => this.removeClass());
    this.stopped = true;
  }
}

export class DynamicClassApplier {

  startMarker: Marker;
  endMarker: Marker;
  classApplier?: EditorClassApplier;
  child?: DynamicClassApplier;

  constructor(
    public editor: Editor,
    public tickerCaller: TickCaller,
    cssClass: string,
    offset: { start: number, end: number },
    silent: boolean = false,
  ) {
    this.startMarker = new Marker(offset.start);
    this.endMarker = new Marker(offset.end);
    editor.core.addMarker(this.startMarker);
    editor.core.addMarker(this.endMarker);

    if (!silent) {
      this.classApplier = new EditorClassApplier(
        editor,
        tickerCaller,
        [`find-replace-${this.startMarker.id}`, cssClass],
        () => ({
          start: this.startMarker.offset,
          end: this.endMarker.offset,
        }),
      );
    }
  }

  clean() {
    this.editor.core.removeMarker(this.startMarker);
    this.editor.core.removeMarker(this.endMarker);

    if (this.classApplier) {
      this.classApplier.stop();
    }
  }
}
