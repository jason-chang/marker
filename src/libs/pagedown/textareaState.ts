import { Chunks } from '@/libs/pagedown/chunks';
import { fixEolChars } from '@/libs/pagedown/util';
import { Editor } from '@/core';

export class TextareaState {

  start!: number;
  end!: number;
  text: string;

  // The input textarea state/contents.
  // This is used to implement undo/redo by the undo manager.\
  constructor(
    public inputArea: Editor,
  ) {
    // Aliases

    this.setInputAreaSelectionStartEnd();
    this.text = this.inputArea.getContent();
  }

  // Sets the selected text in the input box after we've performed an
  // operation.
  setInputAreaSelection() {
    this.inputArea.focus();
    this.inputArea.setSelection(this.start, this.end);
  }

  setInputAreaSelectionStartEnd() {
    this.start = Math.min(
      this.inputArea.selectionMgr.selectionStart,
      this.inputArea.selectionMgr.selectionEnd,
    );
    this.end = Math.max(
      this.inputArea.selectionMgr.selectionStart,
      this.inputArea.selectionMgr.selectionEnd,
    );
  }

  // Restore this state into the input area.
  restore() {

    if (this.text !== undefined && this.text !== this.inputArea.getContent()) {
      this.inputArea.setContent(this.text, false);
    }
    this.setInputAreaSelection();
  }

  // Gets a collection of HTML chunks from the inptut textarea.
  getChunks() {

    const chunk = new Chunks();
    chunk.before = fixEolChars(this.text.substring(0, this.start));
    chunk.startTag = '';
    chunk.selection = fixEolChars(this.text.substring(this.start, this.end));
    chunk.endTag = '';
    chunk.after = fixEolChars(this.text.substring(this.end));

    return chunk;
  }

  // Sets the TextareaState properties given a chunk of markdown.
  setChunks(chunk: Chunks) {

    chunk.before += chunk.startTag;
    chunk.after = chunk.endTag + chunk.after;

    this.start = chunk.before.length;
    this.end = chunk.before.length + chunk.selection.length;
    this.text = chunk.before + chunk.selection + chunk.after;
  }
}
