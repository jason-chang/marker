import { CommandManager } from '@/libs/pagedown/commandManager';
import { UIManager } from '@/libs/pagedown/uIManager';
import { Editor } from '@/core';

export interface PagedownButtonConfig {
  method?: string,
  title?: string,
  icon?: string,
}

export interface PagedownOptions {
  handler?: Function;
  helpButton?: PagedownButtonConfig;
  strings?: {
    [key: string]: string,
  };
  input?: Editor;
}

export const defaultsStrings: { [key: string]: string } = {
  bold: 'Strong <strong> Ctrl/Cmd+B',
  boldexample: 'strong text',

  italic: 'Emphasis <em> Ctrl/Cmd+I',
  italicexample: 'emphasized text',

  strikethrough: 'Strikethrough <s> Ctrl/Cmd+I',
  strikethroughexample: 'strikethrough text',

  link: 'Hyperlink <a> Ctrl/Cmd+L',
  linkdescription: 'enter link description here',
  linkdialog: '<p><b>Insert Hyperlink</b></p><p>http://example.com/ "optional title"</p>',

  quote: 'Blockquote <blockquote> Ctrl/Cmd+Q',
  quoteexample: 'Blockquote',

  code: 'Code Sample <pre><code> Ctrl/Cmd+K',
  codeexample: 'enter code here',

  image: 'Image <img> Ctrl/Cmd+G',
  imagedescription: 'enter image description here',
  imagedialog: "<p><b>Insert Image</b></p><p>http://example.com/images/diagram.jpg \"optional title\"<br><br>Need <a href='http://www.google.com/search?q=free+image+hosting' target='_blank'>free image hosting?</a></p>",

  olist: 'Numbered List <ol> Ctrl/Cmd+O',
  ulist: 'Bulleted List <ul> Ctrl/Cmd+U',
  litem: 'List item',

  heading: 'Heading <h1>/<h2> Ctrl/Cmd+H',
  headingexample: 'Heading',

  hr: 'Horizontal Rule <hr> Ctrl/Cmd+R',

  undo: 'Undo - Ctrl/Cmd+Z',
  redo: 'Redo - Ctrl/Cmd+Y',

  help: 'Markdown Editing Help',
};

function identity(x: string) {
  return x;
}

function returnFalse() {
  return false;
}

export class HookCollection {

  [key: string]: Function;

  chain(hookname: string, func: Function, ...args: any[]) {
    const original = this[hookname];
    if (!original) {
      throw new Error(`unknown hook ${hookname}`);
    }

    if (original === identity) {
      this[hookname] = func;
    } else {
      this[hookname] = () => {
        args = [hookname, func, ...args];
        args[0] = original(...args);
        return func(...args);
      };
    }
  }

  set(hookname: string, func: Function) {
    if (!this[hookname]) {
      throw new Error(`unknown hook ${hookname}`);
    }
    this[hookname] = func;
  }

  addNoop(hookname: string) {
    this[hookname] = identity;
  }

  addFalse(hookname: string) {
    this[hookname] = returnFalse;
  }
}

// options, if given, can have the following properties:
//   options.helpButton = { handler: yourEventHandler }
//   options.strings = { italicexample: "slanted text" }
// `yourEventHandler` is the click handler for the help button.
// If `options.helpButton` isn't given, not help button is created.
// `options.strings` can have any or all of the same properties as
// `defaultStrings` above, so you can just override some string displayed
// to the user on a case-by-case basis, or translate all strings to
// a different language.
//
// For backwards compatibility reasons, the `options` argument can also
// be just the `helpButton` object, and `strings.help` can also be set via
// `helpButton.title`. This should be considered legacy.
//
// The constructed editor object has the methods:
// - getConverter() returns the markdown converter object that was passed to the constructor
// - run() actually starts the editor; should be called after all necessary plugins are registered. Calling this more than once is a no-op.
// - refreshPreview() forces the preview to be updated. This method is only available after run() was called.
export class Pagedown {
  initialized: boolean = false;
  hooks: HookCollection;
  options: PagedownOptions;
  uiManager!: UIManager;

  constructor(userOptions: PagedownOptions | PagedownButtonConfig = {}) {
    let options: PagedownOptions = userOptions as PagedownOptions;
    if (typeof (userOptions as PagedownOptions).handler === 'function') { // backwards compatible behavior
      options = {
        helpButton: (options as PagedownButtonConfig),
      };
    }

    options.strings = options.strings || {};
    this.options = options;

    const hooks = this.hooks = new HookCollection();
    hooks.addNoop('onPreviewRefresh'); // called with no arguments after the preview has been refreshed
    hooks.addNoop('postBlockquoteCreation'); // called with the user's selection *after* the blockquote was created; should return the actual to-be-inserted text
    hooks.addFalse('insertImageDialog');
    /* called with one parameter: a callback to be called with the URL of the image. If the application creates
     * its own image insertion dialog, this hook should return true, and the callback should be called with the chosen
     * image url (or null if the user cancelled). If this hook returns false, the default dialog will be used.
     */
    hooks.addFalse('insertLinkDialog');
  }


  getString(identifier: string) {
    return this.options.strings![identifier] || defaultsStrings[identifier];
  }

  run() {
    if (this.initialized) { return; } // already initialized

    const commandManager = new CommandManager(this.hooks, this.getString.bind(this));
    this.uiManager = new UIManager(this.options.input!, commandManager);
    this.initialized = true;
  }
}
