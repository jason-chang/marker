import { Pagedown, PagedownButtonConfig, PagedownOptions } from '@/libs/pagedown/pagedown';

export default function (options: PagedownOptions | PagedownButtonConfig = {}) {
  return new Pagedown(options);
}
