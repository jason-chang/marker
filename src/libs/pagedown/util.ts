
// Converts \r\n and \r to \n.
export function fixEolChars(text: string) {
  text = text.replace(/\r\n/g, '\n');
  text = text.replace(/\r/g, '\n');
  return text;
}

// Extends a regular expression.  Returns a new RegExp
// using pre + regex + post as the expression.
// Used in a few functions where we have a base
// expression and we want to pre- or append some
// conditions to it (e.g. adding "$" to the end).
// The flags are unchanged.
//
// regex is a RegExp, pre and post are strings.
export function extendRegExp(regex: RegExp, pre: string = '', post: string = '') {

  let pattern = regex.toString();
  let flags;

  // Replace the flags with empty space and store them.
  pattern = pattern.replace(/\/([gim]*)$/, (wholeMatch, flagsPart) => {
    flags = flagsPart;
    return '';
  });

  // Remove the slash delimiters on the regular expression.
  pattern = pattern.replace(/(^\/|\/$)/g, '');
  pattern = pre + pattern + post;

  return new RegExp(pattern, flags);
}
