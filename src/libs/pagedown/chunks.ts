// before: contains all the text in the input box BEFORE the selection.
// after: contains all the text in the input box AFTER the selection.
import { extendRegExp } from '@/libs/pagedown/util';

export class Chunks {
  before!: string;
  after!: string;
  startTag!: string;
  endTag!: string;
  selection!: string;

  // startRegex: a regular expression to find the start tag
  // endRegex: a regular expresssion to find the end tag
  findTags(startRegex?: RegExp | null, endRegex?: RegExp) {

    const chunkObj = this;
    let regex;

    if (startRegex) {

      regex = extendRegExp(startRegex, '', '$');

      this.before = this.before.replace(regex,
        match => {
          chunkObj.startTag += match;
          return '';
        });

      regex = extendRegExp(startRegex, '^', '');

      this.selection = this.selection.replace(regex,
        match => {
          chunkObj.startTag += match;
          return '';
        });
    }

    if (endRegex) {

      regex = extendRegExp(endRegex, '', '$');

      this.selection = this.selection.replace(regex,
        match => {
          chunkObj.endTag = match + chunkObj.endTag;
          return '';
        });

      regex = extendRegExp(endRegex, '^', '');

      this.after = this.after.replace(regex,
        match => {
          chunkObj.endTag = match + chunkObj.endTag;
          return '';
        });
    }
  }

  // If remove is false, the whitespace is transferred
  // to the before/after regions.
  //
  // If remove is true, the whitespace disappears.
  trimWhitespace(remove: boolean = false) {
    const that = this;
    let beforeReplacer;
    let afterReplacer;
    if (remove) {
      beforeReplacer = afterReplacer = '';
    } else {
      beforeReplacer = (s: string) => {
        that.before += s;
        return '';
      };
      afterReplacer = (s: string) => {
        that.after = s + that.after;
        return '';
      };
    }

    this.selection = this.selection
      .replace(/^(\s*)/, beforeReplacer as string)
      .replace(/(\s*)$/, afterReplacer as string);
  }

  skipLines(nLinesBefore = 1, nLinesAfter = 1, findExtraNewlines: boolean = false) {
    nLinesBefore++;
    nLinesAfter++;

    let regexText;
    let replacementText;

    // chrome bug ... documented at: http://meta.stackoverflow.com/questions/63307/blockquote-glitch-in-editor-in-chrome-6-and-7/65985#65985
    if (navigator.userAgent.match(/Chrome/)) {
      'X'.match(/()./);
    }

    this.selection = this.selection.replace(/(^\n*)/, '');

    this.startTag = this.startTag + RegExp.$1;

    this.selection = this.selection.replace(/(\n*$)/, '');
    this.endTag = this.endTag + RegExp.$1;
    this.startTag = this.startTag.replace(/(^\n*)/, '');
    this.before = this.before + RegExp.$1;
    this.endTag = this.endTag.replace(/(\n*$)/, '');
    this.after = this.after + RegExp.$1;

    if (this.before) {

      regexText = replacementText = '';

      while (nLinesBefore--) {
        regexText += '\\n?';
        replacementText += '\n';
      }

      if (findExtraNewlines) {
        regexText = '\\n*';
      }
      this.before = this.before.replace(new RegExp(`${regexText}$`, ''), replacementText);
    }

    if (this.after) {

      regexText = replacementText = '';

      while (nLinesAfter--) {
        regexText += '\\n?';
        replacementText += '\n';
      }
      if (findExtraNewlines) {
        regexText = '\\n*';
      }

      this.after = this.after.replace(new RegExp(regexText, ''), replacementText);
    }
  }
  // end of Chunks
}
