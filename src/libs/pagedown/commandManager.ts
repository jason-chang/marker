/* eslint-disable no-useless-escape, func-names */

import { Chunks } from '@/libs/pagedown/chunks';
import { HookCollection } from '@/libs/pagedown/pagedown';

const SETTINGS = {
  lineLength: 72,
};

// takes the line as entered into the add link/as image dialog and makes
// sure the URL and the optinal title are "nice".
function properlyEncoded(linkdef: string) {
  return linkdef.replace(/^\s*(.*?)(?:\s+"(.+)")?\s*$/, (wholematch: string, link, title) => {
    link = link.replaceAt(
      /\?.*$/,
      (querypart: string) => querypart.replace(/\+/g, ' '), // in the query string, a plus and a space are identical
    );
    link = decodeURIComponent(link); // unencode first, to prevent double encoding
    link = encodeURI(link).replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
    link = link.replaceAt(
      /\?.*$/,
      (querypart: string) => querypart.replace(/\+/g, '%2b'), // since we replaced plus with spaces in the query part, all pluses that now appear where originally encoded
    );
    if (title) {
      title = title.trim ? title.trim() : title.replaceAt(/^\s*/, '').replaceAt(/\s*$/, '');
      title = title.replaceAt(/"/g, 'quot;').replaceAt(/\(/g, '&#40;').replaceAt(/\)/g, '&#41;').replaceAt(/</g, '&lt;')
        .replaceAt(/>/g, '&gt;');
    }
    return title ? `${link} "${title}"` : link;
  });
}

export class CommandManager {

  // The markdown symbols - 4 spaces = code, > = blockquote, etc.
  prefixes: string = '(?:\\s{4,}|\\s*>|\\s*-\\s+|\\s*\\d+\\.|=|\\+|-|_|\\*|#|\\s*\\[[^\n]]+\\]:)';

  [param: string]: any;

  constructor(
    public hooks: HookCollection,
    public getString: Function,
  ) {
    // not empty
  }


  // Remove markdown symbols from the chunk selection.
  unwrap(chunk: Chunks) {
    const txt = new RegExp(`([^\\n])\\n(?!(\\n|${this.prefixes}))`, 'g');
    chunk.selection = chunk.selection.replace(txt, '$1 $2');
  }

  wrap(chunk: Chunks, len: number) {
    this.unwrap(chunk);
    const regex = new RegExp(`(.{1,${len}})( +|$\\n?)`, 'gm');
    const that = this;

    chunk.selection = chunk.selection.replace(regex, (line, marked) => {
      if (new RegExp(`^${that.prefixes}`, '').test(line)) {
        return line;
      }
      return `${marked}\n`;
    });

    chunk.selection = chunk.selection.replace(/\s+$/, '');
  }

  doBold(chunk: Chunks, postProcessing: Function) {
    return this.doBorI(chunk, postProcessing, 2, this.getString('boldexample'));
  }

  doItalic(chunk: Chunks, postProcessing: Function) {
    return this.doBorI(chunk, postProcessing, 1, this.getString('italicexample'));
  }

  // chunk: The selected region that will be enclosed with */**
  // nStars: 1 for italics, 2 for bold
  // insertText: If you just click the button without highlighting text, this gets inserted
  doBorI(chunk: Chunks, postProcessing: Function, nStars: number, insertText: string) {

    // Get rid of whitespace and fixup newlines.
    chunk.trimWhitespace();
    chunk.selection = chunk.selection.replace(/\n{2,}/g, '\n');

    // Look for stars before and after.  Is the chunk already marked up?
    // note that these regex matches cannot fail
    const starsBefore = /(\**$)/.exec(chunk.before)![0];
    const starsAfter = /(^\**)/.exec(chunk.after)![0];

    const prevStars = Math.min(starsBefore.length, starsAfter.length);

    // Remove stars if we have to since the button acts as a toggle.
    if ((prevStars >= nStars) && (prevStars !== 2 || nStars !== 1)) {
      chunk.before = chunk.before.replace(RegExp(`[*]{${nStars}}$`, ''), '');
      chunk.after = chunk.after.replace(RegExp(`^[*]{${nStars}}`, ''), '');
    } else if (!chunk.selection && starsAfter) {
      // It's not really clear why this code is necessary.  It just moves
      // some arbitrary stuff around.
      chunk.after = chunk.after.replace(/^([*_]*)/, '');
      chunk.before = chunk.before.replace(/(\s?)$/, '');
      const whitespace = RegExp.$1;
      chunk.before = chunk.before + starsAfter + whitespace;
    } else {

      // In most cases, if you don't have any selected text and click the button
      // you'll get a selected, marked up region with the default text inserted.
      if (!chunk.selection && !starsAfter) {
        chunk.selection = insertText;
      }

      // Add the true markup.
      const markup = nStars <= 1 ? '*' : '**'; // shouldn't the test be = ?
      chunk.before += markup;
      chunk.after = markup + chunk.after;
    }
  }

  doStrikethrough(chunk: Chunks, postProcessing: Function) {

    // Get rid of whitespace and fixup newlines.
    chunk.trimWhitespace();
    chunk.selection = chunk.selection.replace(/\n{2,}/g, '\n');

    // Look for stars before and after.  Is the chunk already marked up?
    // note that these regex matches cannot fail
    const starsBefore = /(~*$)/.exec(chunk.before)![0];
    const starsAfter = /(^~*)/.exec(chunk.after)![0];

    const prevStars = Math.min(starsBefore.length, starsAfter.length);

    const nStars = 2;

    // Remove stars if we have to since the button acts as a toggle.
    if ((prevStars >= nStars) && (prevStars !== 2)) {
      chunk.before = chunk.before.replace(RegExp(`[~]{${nStars}}$`, ''), '');
      chunk.after = chunk.after.replace(RegExp(`^[~]{${nStars}}`, ''), '');
    } else if (!chunk.selection && starsAfter) {
      // It's not really clear why this code is necessary.  It just moves
      // some arbitrary stuff around.
      chunk.after = chunk.after.replace(/^(~*)/, '');
      chunk.before = chunk.before.replace(/(\s?)$/, '');
      const whitespace = RegExp.$1;
      chunk.before = chunk.before + starsAfter + whitespace;
    } else {

      // In most cases, if you don't have any selected text and click the button
      // you'll get a selected, marked up region with the default text inserted.
      if (!chunk.selection && !starsAfter) {
        chunk.selection = this.getString('strikethroughexample');
      }

      // Add the true markup.
      const markup = '~~'; // shouldn't the test be = ?
      chunk.before += markup;
      chunk.after = markup + chunk.after;
    }
  }

  stripLinkDefs(text: string, defsToAdd: { [key: string]: string }) {
    text = text.replace(/^[ ]{0,3}\[(\d+)]:[ \t]*\n?[ \t]*<?(\S+?)>?[ \t]*\n?[ \t]*(?:(\n*)["(](.+?)[")][ \t]*)?(?:\n+|$)/gm,
      (totalMatch, id, link, newlines, title) => {
        defsToAdd[id] = totalMatch.replace(/\s*$/, '');
        if (newlines) {
          // Strip the title and return that separately.
          defsToAdd[id] = totalMatch.replace(/["(](.+?)[")]$/, '');
          return newlines + title;
        }
        return '';
      });

    return text;
  }

  addLinkDef(chunk: Chunks, linkDef: string = '') {

    let refNumber = 0; // The current reference number
    const defsToAdd: {[key: string]: string} = {}; //
    // Start with a clean slate by removing all previous link definitions.
    chunk.before = this.stripLinkDefs(chunk.before, defsToAdd);
    chunk.selection = this.stripLinkDefs(chunk.selection, defsToAdd);
    chunk.after = this.stripLinkDefs(chunk.after, defsToAdd);

    let defs = '';
    const regex = /(\[)((?:\[[^\]]*]|[^\[\]])*)(][ ]?(?:\n[ ]*)?\[)(\d+)(])/g;

    const addDefNumber = function (def: string) {
      refNumber++;
      def = def.replace(/^[ ]{0,3}\[(\d+)]:/, `  [${refNumber}]:`);
      defs += `\n${def}`;
    };

    // note that
    // a) the recursive call to getLink cannot go infinite, because by definition
    //    of regex, inner is always a proper substring of wholeMatch, and
    // b) more than one level of nesting is neither supported by the regex
    //    nor making a lot of sense (the only use case for nesting is a linked image)
    const getLink = function (wholeMatch: string, before: string, inner: string, afterInner: string, id: string, end: string) {
      inner = inner.replace(regex, getLink);
      if (defsToAdd[id]) {
        addDefNumber(defsToAdd[id]);
        return before + inner + afterInner + refNumber + end;
      }
      return wholeMatch;
    };

    chunk.before = chunk.before.replace(regex, getLink);

    if (linkDef) {
      addDefNumber(linkDef);
    } else {
      chunk.selection = chunk.selection.replace(regex, getLink);
    }

    const refOut = refNumber;

    chunk.after = chunk.after.replace(regex, getLink);

    if (chunk.after) {
      chunk.after = chunk.after.replace(/\n*$/, '');
    }
    if (!chunk.after) {
      chunk.selection = chunk.selection.replace(/\n*$/, '');
    }

    chunk.after += `\n\n${defs}`;

    return refOut;
  }

  doLinkOrImage(chunk: Chunks, postProcessing: Function, isImage: boolean = false) {

    chunk.trimWhitespace();
    // chunk.findTags(/\s*!?\[/, /\][ ]?(?:\n[ ]*)?(\[.*?\])?/);
    chunk.findTags(/\s*!?\[/, /][ ]?(?:\n[ ]*)?(\(.*?\))?/);

    if (chunk.endTag.length > 1 && chunk.startTag.length > 0) {
      chunk.startTag = chunk.startTag.replace(/!?\[/, '');
      chunk.endTag = '';
      this.addLinkDef(chunk);
      return false;
    }

    // We're moving start and end tag back into the selection, since (as we're in the else block) we're not
    // *removing* a link, but *adding* one, so whatever findTags() found is now back to being part of the
    // link text. linkEnteredCallback takes care of escaping any brackets.
    chunk.selection = chunk.startTag + chunk.selection + chunk.endTag;
    chunk.startTag = chunk.endTag = '';

    if (/\n\n/.test(chunk.selection)) {
      this.addLinkDef(chunk);
      return false;
    }
    const that = this;
    // The function to be executed when you enter a link and press OK or Cancel.
    // Marks up the link and adds the ref.
    const linkEnteredCallback = function (link: string) {

      if (link !== null) {
        // (                          $1
        //     [^\\]                  anything that's not a backslash
        //     (?:\\\\)*              an even number (this includes zero) of backslashes
        // )
        // (?=                        followed by
        //     [[\]]                  an opening or closing bracket
        // )
        //
        // In other words, a non-escaped bracket. These have to be escaped now to make sure they
        // don't count as the end of the link or similar.
        // Note that the actual bracket has to be a lookahead, because (in case of to subsequent brackets),
        // the bracket in one match may be the "not a backslash" character in the next match, so it
        // should not be consumed by the first match.
        // The "prepend a space and finally remove it" steps makes sure there is a "not a backslash" at the
        // start of the string, so this also works if the selection begins with a bracket. We cannot solve
        // this by anchoring with ^, because in the case that the selection starts with two brackets, this
        // would mean a zero-width match at the start. Since zero-width matches advance the string position,
        // the first bracket could then not act as the "not a backslash" for the second.
        chunk.selection = (` ${chunk.selection}`).replace(/([^\\](?:\\\\)*)(?=[[\]])/g, '$1\\').substr(1);

        /*
        var linkDef = " [999]: " + properlyEncoded(link);

        var num = that.addLinkDef(chunk, linkDef);
        */
        chunk.startTag = isImage ? '![' : '[';
        // chunk.endTag = "][" + num + "]";
        chunk.endTag = `](${properlyEncoded(link)})`;

        if (!chunk.selection) {
          if (isImage) {
            chunk.selection = that.getString('imagedescription');
          } else {
            chunk.selection = that.getString('linkdescription');
          }
        }
      }
      postProcessing();
    };

    if (isImage) {
      this.hooks.insertImageDialog(linkEnteredCallback);
    } else {
      this.hooks.insertLinkDialog(linkEnteredCallback);
    }
    return true;
  }

  // When making a list, hitting shift-enter will put your cursor on the next line
  // at the current indent level.
  doAutoindent(chunk: Chunks) {

    const commandMgr = this;
    let fakeSelection = false;

    chunk.before = chunk.before.replace(/(\n|^)[ ]{0,3}([*+-]|\d+[.])[ \t]*\n$/, '\n\n');
    chunk.before = chunk.before.replace(/(\n|^)[ ]{0,3}>[ \t]*\n$/, '\n\n');
    chunk.before = chunk.before.replace(/(\n|^)[ \t]+\n$/, '\n\n');

    // There's no selection, end the cursor wasn't at the end of the line:
    // The user wants to split the current list item / code line / blockquote line
    // (for the latter it doesn't really matter) in two. Temporarily select the
    // (rest of the) line to achieve this.
    if (!chunk.selection && !/^[ \t]*(?:\n|$)/.test(chunk.after)) {
      chunk.after = chunk.after.replace(/^[^\n]*/, wholeMatch => {
        chunk.selection = wholeMatch;
        return '';
      });
      fakeSelection = true;
    }

    if (/(\n|^)[ ]{0,3}([*+-]|\d+[.])[ \t]+.*\n$/.test(chunk.before)) {
      if (commandMgr.doList) {
        commandMgr.doList(chunk);
      }
    }
    if (/(\n|^)[ ]{0,3}>[ \t]+.*\n$/.test(chunk.before)) {
      if (commandMgr.doBlockquote) {
        commandMgr.doBlockquote(chunk);
      }
    }
    if (/(\n|^)(\t|[ ]{4,}).*\n$/.test(chunk.before)) {
      if (commandMgr.doCode) {
        commandMgr.doCode(chunk);
      }
    }

    if (fakeSelection) {
      chunk.after = chunk.selection + chunk.after;
      chunk.selection = '';
    }
  }

  doBlockquote(chunk: Chunks) {

    chunk.selection = chunk.selection.replace(/^(\n*)([^\r]+?)(\n*)$/,
      (totalMatch, newlinesBefore, text, newlinesAfter) => {
        chunk.before += newlinesBefore;
        chunk.after = newlinesAfter + chunk.after;
        return text;
      });

    chunk.before = chunk.before.replace(/(>[ \t]*)$/,
      (totalMatch, blankLine) => {
        chunk.selection = blankLine + chunk.selection;
        return '';
      });

    chunk.selection = chunk.selection.replace(/^(\s|>)+$/, '');
    chunk.selection = chunk.selection || this.getString('quoteexample');

    // The original code uses a regular expression to find out how much of the
    // text *directly before* the selection already was a blockquote:

    /*
    if (chunk.before) {
    chunk.before = chunk.before.replace(/\n?$/, "\n");
    }
    chunk.before = chunk.before.replace(/(((\n|^)(\n[ \t]*)*>(.+\n)*.*)+(\n[ \t]*)*$)/,
    function (totalMatch) {
    chunk.startTag = totalMatch;
    return "";
    });
    */

    // This comes down to:
    // Go backwards as many lines a possible, such that each line
    //  a) starts with ">", or
    //  b) is almost empty, except for whitespace, or
    //  c) is preceeded by an unbroken chain of non-empty lines
    //     leading up to a line that starts with ">" and at least one more character
    // and in addition
    //  d) at least one line fulfills a)
    //
    // Since this is essentially a backwards-moving regex, it's susceptible to
    // catstrophic backtracking and can cause the browser to hang;
    // see e.g. http://meta.stackoverflow.com/questions/9807.
    //
    // Hence we replaced this by a simple state machine that just goes through the
    // lines and checks for a), b), and c).

    let match = '';
    let leftOver = '';
    let line;
    if (chunk.before) {
      const lines = chunk.before.replace(/\n$/, '').split('\n');
      let inChain = false;
      for (let i = 0; i < lines.length; i++) {
        let good = false;
        line = lines[i];
        inChain = inChain && line.length > 0; // c) any non-empty line continues the chain
        if (/^>/.test(line)) { // a)
          good = true;
          if (!inChain && line.length > 1) { // c) any line that starts with ">" and has at least one more character starts the chain
            inChain = true;
          }
        } else if (/^[ \t]*$/.test(line)) { // b)
          good = true;
        } else {
          good = inChain; // c) the line is not empty and does not start with ">", so it matches if and only if we're in the chain
        }
        if (good) {
          match += `${line}\n`;
        } else {
          leftOver += match + line;
          match = '\n';
        }
      }
      if (!/(^|\n)>/.test(match)) { // d)
        leftOver += match;
        match = '';
      }
    }

    chunk.startTag = match;
    chunk.before = leftOver;

    // end of change

    if (chunk.after) {
      chunk.after = chunk.after.replace(/^\n?/, '\n');
    }

    chunk.after = chunk.after.replace(/^(((\n|^)(\n[ \t]*)*>(.+\n)*.*)+(\n[ \t]*)*)/,
      totalMatch => {
        chunk.endTag = totalMatch;
        return '';
      });

    const replaceBlanksInTags = function (useBracket: boolean = false) {

      const replacement = useBracket ? '> ' : '';

      if (chunk.startTag) {
        chunk.startTag = chunk.startTag.replace(/\n((>|\s)*)\n$/,
          (totalMatch, markdown) => `\n${markdown.replaceAt(/^[ ]{0,3}>?[ \t]*$/gm, replacement)}\n`);
      }
      if (chunk.endTag) {
        chunk.endTag = chunk.endTag.replace(/^\n((>|\s)*)\n/,
          (totalMatch, markdown) => `\n${markdown.replaceAt(/^[ ]{0,3}>?[ \t]*$/gm, replacement)}\n`);
      }
    };

    if (/^(?![ ]{0,3}>)/m.test(chunk.selection)) {
      this.wrap(chunk, SETTINGS.lineLength - 2);
      chunk.selection = chunk.selection.replace(/^/gm, '> ');
      replaceBlanksInTags(true);
      chunk.skipLines();
    } else {
      chunk.selection = chunk.selection.replace(/^[ ]{0,3}> ?/gm, '');
      this.unwrap(chunk);
      replaceBlanksInTags(false);

      if (!/^(\n|^)[ ]{0,3}>/.test(chunk.selection) && chunk.startTag) {
        chunk.startTag = chunk.startTag.replace(/\n{0,2}$/, '\n\n');
      }

      if (!/(\n|^)[ ]{0,3}>.*$/.test(chunk.selection) && chunk.endTag) {
        chunk.endTag = chunk.endTag.replace(/^\n{0,2}/, '\n\n');
      }
    }

    chunk.selection = this.hooks.postBlockquoteCreation(chunk.selection);

    if (!/\n/.test(chunk.selection)) {
      chunk.selection = chunk.selection.replace(/^(> *)/,
        (wholeMatch, blanks) => {
          chunk.startTag += blanks;
          return '';
        });
    }
  }

  doCode(chunk: Chunks) {

    const hasTextBefore = /\S[ ]*$/.test(chunk.before);
    const hasTextAfter = /^[ ]*\S/.test(chunk.after);

    // Use 'four space' markdown if the selection is on its own
    // line or is multiline.
    if ((!hasTextAfter && !hasTextBefore) || /\n/.test(chunk.selection)) {

      chunk.before = chunk.before.replace(/[ ]{4}$/,
        totalMatch => {
          chunk.selection = totalMatch + chunk.selection;
          return '';
        });

      let nLinesBack = 1;
      let nLinesForward = 1;

      if (/(\n|^)(\t|[ ]{4,}).*\n$/.test(chunk.before)) {
        nLinesBack = 0;
      }
      if (/^\n(\t|[ ]{4,})/.test(chunk.after)) {
        nLinesForward = 0;
      }

      chunk.skipLines(nLinesBack, nLinesForward);

      if (!chunk.selection) {
        chunk.startTag = '    ';
        chunk.selection = this.getString('codeexample');
      } else if (/^[ ]{0,3}\S/m.test(chunk.selection)) {
        if (/\n/.test(chunk.selection)) {
          chunk.selection = chunk.selection.replace(/^/gm, '    ');
        } else {
          // if it's not multiline, do not select the four added spaces; this is more consistent with the doList behavior
          chunk.before += '    ';
        }
      } else {
        chunk.selection = chunk.selection.replace(/^(?:[ ]{4}|[ ]{0,3}\t)/gm, '');
      }
    } else {
      // Use backticks (`) to delimit the code block.

      chunk.trimWhitespace();
      chunk.findTags(/`/, /`/);

      if (!chunk.startTag && !chunk.endTag) {
        chunk.startTag = chunk.endTag = '`';
        if (!chunk.selection) {
          chunk.selection = this.getString('codeexample');
        }
      } else if (chunk.endTag && !chunk.startTag) {
        chunk.before += chunk.endTag;
        chunk.endTag = '';
      } else {
        chunk.startTag = chunk.endTag = '';
      }
    }
  }

  doList(chunk: Chunks, postProcessing?: Function, isNumberedList?: boolean, isCheckList?: boolean) {

    // These are identical except at the very beginning and end.
    // Should probably use the regex extension function to make this clearer.
    const previousItemsRegex = /(\n|^)(([ ]{0,3}([*+-]|\d+[.])[ \t]+.*)(\n.+|\n{2,}([*+-].*|\d+[.])[ \t]+.*|\n{2,}[ \t]+\S.*)*)\n*$/;
    const nextItemsRegex = /^\n*(([ ]{0,3}([*+-]|\d+[.])[ \t]+.*)(\n.+|\n{2,}([*+-].*|\d+[.])[ \t]+.*|\n{2,}[ \t]+\S.*)*)\n*/;

    // The default bullet is a dash but others are possible.
    // This has nothing to do with the particular HTML bullet,
    // it's just a markdown bullet.
    let bullet = '-';

    // The number in a numbered list.
    let num = 1;

    // Get the item prefix - e.g. " 1. " for a numbered list, " - " for a bulleted list.
    const getItemPrefix = function (checkListContent?: string) {
      let prefix;
      if (isNumberedList) {
        prefix = ` ${num}. `;
        num++;
      } else {
        prefix = ` ${bullet} `;
        if (isCheckList) {
          prefix += '[';
          prefix += checkListContent || ' ';
          prefix += '] ';
        }
      }
      return prefix;
    };

    // Fixes the prefixes of the other list items.
    const getPrefixedItem = function (itemText: string = '') {

      // The numbering flag is unset when called by autoindent.
      if (isNumberedList === undefined) {
        isNumberedList = /^\s*\d/.test(itemText);
      }

      // Renumber/bullet the list element.
      itemText = itemText.replace(
        isCheckList
          ? /^[ ]{0,3}([*+-]|\d+[.])\s+\[([ xX])]\s/gm
          : /^[ ]{0,3}([*+-]|\d+[.])\s/gm,
        (match, p1, p2) => getItemPrefix(p2),
      );

      return itemText;
    };

    chunk.findTags(/(\n|^)*[ ]{0,3}([*+-]|\d+[.])\s+/);

    if (chunk.before && !/\n$/.test(chunk.before) && !/^\n/.test(chunk.startTag)) {
      chunk.before += chunk.startTag;
      chunk.startTag = '';
    }

    if (chunk.startTag) {

      const hasDigits = /\d+[.]/.test(chunk.startTag);
      chunk.startTag = '';
      chunk.selection = chunk.selection.replace(/\n[ ]{4}/g, '\n');
      this.unwrap(chunk);
      chunk.skipLines();

      if (hasDigits) {
        // Have to renumber the bullet points if this is a numbered list.
        chunk.after = chunk.after.replace(nextItemsRegex, getPrefixedItem);
      }
      if (isNumberedList === hasDigits) {
        return;
      }
    }

    let nLinesUp = 1;

    chunk.before = chunk.before.replace(previousItemsRegex,
      itemText => {
        if (/^\s*([*+-])/.test(itemText)) {
          bullet = RegExp.$1;
        }
        nLinesUp = /[^\n]\n\n[^\n]/.test(itemText) ? 1 : 0;
        return getPrefixedItem(itemText);
      });

    if (!chunk.selection) {
      chunk.selection = this.getString('litem');
    }

    const prefix = getItemPrefix();

    let nLinesDown = 1;

    chunk.after = chunk.after.replace(nextItemsRegex,
      itemText => {
        nLinesDown = /[^\n]\n\n[^\n]/.test(itemText) ? 1 : 0;
        return getPrefixedItem(itemText);
      });

    chunk.trimWhitespace(true);
    chunk.skipLines(nLinesUp, nLinesDown, true);
    chunk.startTag = prefix;
    const spaces = prefix.replace(/./g, ' ');
    this.wrap(chunk, SETTINGS.lineLength - spaces.length);
    chunk.selection = chunk.selection.replace(/\n/g, `\n${spaces}`);
  }

  doTable(chunk: Chunks) {
    // Credit: https://github.com/fcrespo82/atom-markdown-table-formatter

    const keepFirstAndLastPipes = true;
    /*
      ( # header capture
        (?:
          (?:[^\n]*?\|[^\n]*)       # line w/ at least one pipe
          \ *                       # maybe trailing whitespace
        )?                          # maybe header
        (?:\n|^)                    # newline
      )
      ( # format capture
        (?:
          \|\ *:?-+:?\ *            # format starting w/pipe
          |\|?(?:\ *:?-+:?\ *\|)+   # or separated by pipe
        )
        (?:\ *:?-+:?\ *)?           # maybe w/o trailing pipe
        \ *                         # maybe trailing whitespace
        \n                          # newline
      )
      ( # body capture
        (?:
          (?:[^\n]*?\|[^\n]*)       # line w/ at least one pipe
          \ *                       # maybe trailing whitespace
          (?:\n|$)                  # newline
        )+ # at least one
      )
    */
    const regex = /((?:(?:[^\n]*?\|[^\n]*) *)?(?:\r?\n|^))((?:\| *:?-+:? *|\|?(?: *:?-+:? *\|)+)(?: *:?-+:? *)? *\r?\n)((?:(?:[^\n]*?\|[^\n]*) *(?:\r?\n|$))+)/;


    function padding(len: number, str?: string) {
      let result = '';
      str = str || ' ';
      len = Math.floor(len);
      for (let i = 0; i < len; i++) {
        result += str;
      }
      return result;
    }

    function stripTailPipes(str: string = '') {
      return str.trim().replace(/(^\||\|$)/g, '');
    }

    function splitCells(str: string = '') {
      return str.split('|');
    }

    function addTailPipes(str: string = '') {
      if (keepFirstAndLastPipes) {
        return `|${str}|`;
      }
      return str;

    }

    function joinCells(arr: string[]) {
      return arr.join('|');
    }

    function formatTable(text: RegExpMatchArray, appendNewline: boolean = false) {
      const headerline = text[1].trim();
      let i;
      let j;
      let len1;
      const ref1 = headerline.length === 0 ? [0, text[3]] : [1, text[1] + text[3]];
      let ref2;
      let ref3;
      let k;
      let len2;
      let results;
      let formatline;
      const formatrow = ref1[0];
      const data = ref1[1];
      let line;
      const lines = (data as string).trim().split('\n');
      const justify: string[] = [];
      let cell;
      let cells;
      let first;
      let last;
      let ends;
      let columns = justify.length;
      const content = [];
      const widths: number[] = [];
      const formatted = [];
      let front;
      let back;
      formatline = text[2].trim();
      ref2 = splitCells(stripTailPipes(formatline));
      for (j = 0, len1 = ref2.length; j < len1; j++) {
        cell = ref2[j];
        ref3 = cell.trim();
        first = ref3[0];
        last = ref3[ref3.length - 1];
        switch ((ends = (first || ':') + (last || ''))) {
          case '::':
          case '-:':
          case ':-':
            justify.push(ends);
            break;
          default:
            justify.push('--');
        }
      }
      columns = justify.length;
      for (j = 0, len1 = lines.length; j < len1; j++) {
        line = lines[j];
        cells = splitCells(stripTailPipes(line));
        cells[columns - 1] = joinCells(cells.slice(columns - 1));
        results = [];
        for (k = 0, len2 = cells.length; k < len2; k++) {
          cell = cells[k];
          ref2 = typeof cell === 'string' ? cell.trim() : undefined;
          results.push(padding(1, ' ') + (ref2 || '') + padding(1, ' '));
        }
        content.push(results);
      }

      for (i = j = 0, ref2 = columns - 1; ref2 >= 0 ? j <= ref2 : j >= ref2; i = ref2 >= 0 ? ++j : --j) {
        results = [];
        for (k = 0, len1 = content.length; k < len1; k++) {
          cells = content[k];
          results.push(cells[i].length);
        }
        widths.push(Math.max(...[2].concat(results)));
      }

      const just = function (string: string, col: number) {
        let back; let front;
        const length = widths[col] - string.length;
        switch (justify[col]) {
          case '::':
            front = padding(0);
            back = padding(1);
            return padding(length / 2) + string + padding((length + 1) / 2);
          case '-:':
            return padding(length) + string;
          default:
            return string + padding(length);
        }
      };

      for (j = 0, len1 = content.length; j < len1; j++) {
        cells = content[j];
        results = [];
        for (i = k = 0, ref2 = columns - 1; ref2 >= 0 ? k <= ref2 : k >= ref2; i = ref2 >= 0 ? ++k : --k) {
          results.push(just(cells[i], i));
        }
        formatted.push(addTailPipes(joinCells(results)));
      }
      formatline = addTailPipes(joinCells((function () {
        let j; let ref2; let ref3;
        const results = [];
        for (i = j = 0, ref2 = columns - 1; ref2 >= 0 ? j <= ref2 : j >= ref2; i = ref2 >= 0 ? ++j : --j) {
          ref3 = justify[i];
          front = ref3[0];
          back = ref3[1];
          results.push(front + padding(widths[i] - 2, '-') + back);
        }
        return results;
      })()));
      formatted.splice((formatrow as number), 0, formatline);
      let result = (headerline.length === 0 && text[1] !== '' ? '\n' : '') + formatted.join('\n');
      if (appendNewline !== false) {
        result += '\n';
      }
      return result;
    }

    if (chunk.before.slice(-1) !== '\n') {
      chunk.before += '\n';
    }
    let match = chunk.selection.match(regex);
    if (match) {
      chunk.selection = formatTable(match, chunk.selection.slice(-1) === '\n');
    } else {
      let table = `${chunk.selection}|\n-|-\n|`;
      match = table.match(regex);
      if (!match || match[0].slice(0, table.length) !== table) {
        return;
      }
      table = formatTable(match);
      const selectionOffset = keepFirstAndLastPipes ? 1 : 0;
      const pipePos = table.indexOf('|', selectionOffset);
      chunk.before += table.slice(0, selectionOffset);
      chunk.selection = table.slice(selectionOffset, pipePos);
      chunk.after = table.slice(pipePos) + chunk.after;
    }
  }

  doHeading(chunk: Chunks) {

    // Remove leading/trailing whitespace and reduce internal spaces to single spaces.
    chunk.selection = chunk.selection.replace(/\s+/g, ' ');
    chunk.selection = chunk.selection.replace(/(^\s+|\s+$)/g, '');

    // If we clicked the button with no selected text, we just
    // make a level 2 hash header around some default text.
    if (!chunk.selection) {
      chunk.startTag = '## ';
      chunk.selection = this.getString('headingexample');
      return;
    }

    let headerLevel = 0; // The existing header level of the selected text.

    // Remove any existing hash heading markdown and save the header level.
    chunk.findTags(/#+[ ]*/, /[ ]*#+/);
    if (/#+/.test(chunk.startTag)) {
      headerLevel = RegExp.lastMatch.length;
    }
    chunk.startTag = chunk.endTag = '';

    // Try to get the current header level by looking for - and = in the line
    // below the selection.
    chunk.findTags(null, /\s?(-+|=+)/);
    if (/=+/.test(chunk.endTag)) {
      headerLevel = 1;
    }
    if (/-+/.test(chunk.endTag)) {
      headerLevel = 2;
    }

    // Skip to the next line so we can create the header markdown.
    chunk.startTag = chunk.endTag = '';
    chunk.skipLines(1, 1);

    // We make a level 2 header if there is no current header.
    // If there is a header level, we substract one from the header level.
    // If it's already a level 1 header, it's removed.
    let headerLevelToCreate = headerLevel === 0 ? 2 : headerLevel - 1;

    if (headerLevelToCreate > 0) {

      chunk.startTag = '';
      while (headerLevelToCreate--) {
        chunk.startTag += '#';
      }
      chunk.startTag += ' ';
    }
  }

  doHorizontalRule(chunk: Chunks) {
    chunk.startTag = '----------\n';
    chunk.selection = '';
    chunk.skipLines(2, 1, true);
  }
}
