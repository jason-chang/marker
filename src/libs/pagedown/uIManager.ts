/* eslint-disable func-names */
import { TextareaState } from '@/libs/pagedown/textareaState';
import { Editor } from '@/core';
import { CommandManager } from '@/libs/pagedown/commandManager';
import { Chunks } from '@/libs/pagedown/chunks';

export class UIManager {

  buttons: {
    [key: string]: (chunk: Chunks, postProcessing: Function) => any
  } = {}; // buttons.undo, buttons.link, etc. The actual DOM elements.

  constructor(
    public inputBox: Editor,
    public commandManager: CommandManager,
  ) {
    this.makeSpritedButtonRow();
  }

  // Perform the button's action.
  doClick(buttonName: string) {
    const button = this.buttons[buttonName];
    if (!button) {
      return;
    }

    this.inputBox.focus();
    const linkOrImage = button === this.buttons.link || (button as any).id === this.buttons.image;

    const state = new TextareaState(this.inputBox);

    if (!state) {
      return;
    }

    const chunks = state.getChunks();

    // Some commands launch a "modal" prompt dialog.  Javascript
    // can't really make a modal dialog box and the WMD code
    // will continue to execute while the dialog is displayed.
    // This prevents the dialog pattern I'm used to and means
    // I can't do something like this:
    //
    // var link = CreateLinkDialog();
    // makeMarkdownLink(link);
    //
    // Instead of this straightforward method of handling a
    // dialog I have to pass any code which would execute
    // after the dialog is dismissed (e.g. link creation)
    // in a function parameter.
    //
    // Yes this is awkward and I think it sucks, but there's
    // no real workaround.  Only the image and link code
    // create dialogs and require the function pointers.
    const fixupInputArea = () => {

      this.inputBox.focus();

      if (chunks) {
        state.setChunks(chunks);
      }

      state.restore();
    };

    const noCleanup = button(chunks, fixupInputArea);

    if (!noCleanup) {
      fixupInputArea();
      if (!linkOrImage) {
        this.inputBox.adjustCursorPosition();
      }
    }
  }


  bindCommand(method?: string | Function) {
    if (typeof method === 'string') {
      method = this.commandManager[method];
    }

    return (chunk: Chunks, postProcessing: Function, ...args: any[]) => {
      (method as Function).apply(this.commandManager, [chunk, postProcessing, ...args]);
    };
  }

  makeSpritedButtonRow() {

    this.buttons.bold = this.bindCommand('doBold');
    this.buttons.italic = this.bindCommand('doItalic');
    this.buttons.strikethrough = this.bindCommand('doStrikethrough');
    this.buttons.link = this.bindCommand(function (this: CommandManager, chunk: Chunks, postProcessing: Function) {
      return this.doLinkOrImage(chunk, postProcessing, false);
    });
    this.buttons.quote = this.bindCommand('doBlockquote');
    this.buttons.code = this.bindCommand('doCode');
    this.buttons.image = this.bindCommand(function (this: CommandManager, chunk: Chunks, postProcessing: Function) {
      return this.doLinkOrImage(chunk, postProcessing, true);
    });
    this.buttons.olist = this.bindCommand(function (this: CommandManager, chunk: Chunks, postProcessing: Function) {
      this.doList(chunk, postProcessing, true);
    });
    this.buttons.ulist = this.bindCommand(function (this: CommandManager, chunk: Chunks, postProcessing: Function) {
      this.doList(chunk, postProcessing, false);
    });
    this.buttons.clist = this.bindCommand(function (this: CommandManager, chunk: Chunks, postProcessing: Function) {
      this.doList(chunk, postProcessing, false, true);
    });
    this.buttons.heading = this.bindCommand('doHeading');
    this.buttons.hr = this.bindCommand('doHorizontalRule');
    this.buttons.table = this.bindCommand('doTable');
  }

}
